PROVIDER ?= ft1.bitmask.net

clean-build-container:
	# clear cached images
	podman rm menshen -f
	podman build -t menshen -f Dockerfile

build:
	go build ./cmd/menshen
swag:
	swag init -g cmd/menshen/main.go --output api
	swagger generate client -f api/swagger.yaml
	go mod tidy

test-swag:
	swag init -g cmd/menshen/main.go --output api
	swagger generate client -f api/swagger.yaml
	if git diff --quiet client models api; then echo "Swagger check passed"; else git --no-pager diff && echo "Generated Swagger files not committed"; exit 1; fi

test-swag-on-container:
	# test against the same versions as on CI
	podman run --rm -v $(shell pwd):/app -w /app docker.io/library/golang:alpine \
				apk add build-base &&\
				go install github.com/swaggo/swag/cmd/swag@latest && \
				go install github.com/go-swagger/go-swagger/cmd/swagger@latest &&\
				swag init -g cmd/menshen/main.go --output api &&\
				swagger generate client -f api/swagger.yaml
	if git diff --quiet client models api; then echo "Swagger check passed"; else git --no-pager diff && echo "Generated Swagger files not committed"; exit 1; fi

rebuild-and-integration-test: clean-container-setup clean-build-container run-container-test-api

integration-test: clean-container-setup run-container-test-api

run-container-test-api:
	# run container image in all three modes and validate the cert-generation endpoint
	# 1. run container image with local config and certs
	podman-compose -f test/compose-files/docker-compose.yml up -d
	sleep 5s
	curl localhost:8443/api/5/openvpn/cert > /tmp/v5cert.pem
	podman logs compose-files_menshen_1
	openssl verify -CAfile test/data/ovpn_client_ca.crt /tmp/v5cert.pem
	echo "removing downloaded v5 cert" && rm /tmp/v5cert.pem
	echo "trying to fetch v3 rsa cert"
	curl localhost:8443/3/cert > /tmp/v3cert.pem
	openssl x509 -in /tmp/v3cert.pem -noout -text
	openssl verify -CAfile test/data/ovpn_client_ca.crt /tmp/v3cert.pem
	echo "removing downloaded v3 cert" && rm /tmp/v3cert.pem
	podman logs compose-files_menshen_1
	podman-compose -f test/compose-files/docker-compose.yml down
	# 2. run container image with remote selfsigned url's for fetching config and certs
	curl -o test/data/provider.crt https://${PROVIDER}/ca.crt
	podman-compose -f test/compose-files/docker-compose-remote-client-cert-selfsigned.yml up -d
	sleep 5s
	curl localhost:8443/api/5/openvpn/cert > /tmp/cert.pem
	podman logs compose-files_menshen_1
	# check the validity of certificate 
	openssl x509 -in /tmp/cert.pem -noout -text
	podman-compose -f test/compose-files/docker-compose-remote-client-cert-selfsigned.yml down

clean-container-setup:
	# clean local menshen setup 
	containers=$$(podman ps -aq --filter "name=compose-files_menshen_1"); \
	for container in $$containers; do \
		podman rm -f $$container; \
	done
