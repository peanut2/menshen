package models

import "strings"

type Location struct {
	// CountryCode is the two-character country ISO identifier (uppercase).
	CountryCode string `json:"country_code"`
	// DisplayName is the user-facing string for a given location.
	DisplayName string `json:"display_name"`
	// Any location that has at least one bridge configured will set this to true.
	HasBridges bool `json:"has_bridges"`
	// TODO Not used right now, but intended to signal when a location has all of their
	// nodes overwhelmed.
	Healthy bool `json:"healthy"`
	// Hemisphere is a legacy label for a gateway. The rationale was once
	// intended to be to allocate gateways for an hemisphere with certain
	// regional "fairness", even if they're geographically located in a
	// different region. We might want to set this on the Gateway or Bridge, not in the
	// Location itself...
	Hemisphere string `json:"hemisphere"`
	// Label is the short representation of a location, used internally.
	Label string `json:"label"`
	// Lat is the latitude for the location.
	Lat string `json:"lat"`
	// Lon is the longitude for the location.
	Lon string `json:"lon"`
	// Region is the continental region this gateway is assigned to. Not used at the moment,
	// intended to use a label from the 7-continent model.
	Region string `json:"region"`
	// Timezone is the TZ for the location (-1, 0, +1, ...)
	Timezone string `json:"timezone"`
}

// CanonicalizeLocation returns a lowercase, no-space representation of the passed string.
func CanonicalizeLocation(loc string) string {
	l := strings.ToLower(loc)
	return strings.Replace(l, " ", "", -1)
}

// EndpointFilter is a function (parametrized for Gateway or Bridge) that takes a pointer to Endpoint and returns bool. It is used to pass filtering functions around.
type EndpointFilter[E Gateway | Bridge] func(*E) bool

// Gateway is the main endpoint for the tunneling protocol.
type Gateway struct {
	// An experimental gateway flags any gateway that, for whatever reason,
	// is not deemed stable. The expectation is that clients have to opt-in to
	// experimental gateways (and bridges too).
	Experimental bool `json:"experimental"`
	// Not used now - we could potentially flag gateways that are planned
	// to undergo maintenance mode some time in advance.
	// We can also automatically flag as not healthy gateways that appear
	// not to be routing to the internet.
	Healthy bool `json:"healthy"`
	// Host is a unique identifier for the gateway host. It does not need to resolve, since
	// we're not using DNS to resolve the gateways.
	Host string `json:"host"`
	// IPAddr is the IPv4 address
	IPAddr string `json:"ip_addr"`
	// IP6Addr is the IPv6 address
	IP6Addr string `json:"ip6_addr"`
	// Load is the fractional load received from the menshen agent. For the
	// time being it is a synthethic metric that takes into account number of clients
	// and network information for the node.
	Load float64 `json:"load"`
	// Location is the canonical label for the location of the gateway.
	Location string `json:"location"`
	// Overloaded should be set to true if the fractional load is above threshold.
	Overloaded bool `json:"overloaded"`
	// The (primary) port this gateway is listening on.
	Port int `json:"port"`
	// TCP, UDP or KCP. This was called "protocol" in previous versions of the API.
	Transport string `json:"transport"`
	// Type is the type of gateway. The only valid type as of 2023 is openvpn.
	Type string `json:"type"`
	// Bucket is a "bucket" tag that connotes a resource group that a user may or may not
	// have access to. An empty bucket string implies that it is open access
	Bucket string `json:"bucket"`
	// LastSeenMillis is a unix time in milliseconds representing the last time we received a heartbeat update from this gateway
	LastSeenMillis int64 `json:"last_seen_millis" format:"int64"`
}

// Bridge is a (encrypted) proxy that is configured to connect to a Gateway.
type Bridge struct {
	// Any authentication method needed for connect to the bridge, `none`
	// otherwise.
	Auth string `json:"auth,omitempty"`
	// An experimental bridge flags any bridge that, for whatever reason,
	// is not deemed stable. The expectation is that clients have to opt-in to
	// experimental bridges (and gateways too).
	Experimental bool `json:"experimental,omitempty"`
	// Healthy indicates whether this bridge can be used normally.
	Healthy bool `json:"healthy"`
	// Host is a unique identifier for the bridge.
	Host string `json:"host,omitempty"`
	// IPAddr is the IPv4 address
	IPAddr string `json:"ip_addr"`
	// IP6Addr is the IPv6 address
	IP6Addr string `json:"ip6_addr"`
	// Load is the fractional load - but for now menshen agent is not measuring
	// load in the bridges.
	Load float32 `json:"load"`
	// Location refers to the location to which this bridge points to
	Location string `json:"location"`
	// Overloaded should be set to true if the fractional load is above threshold.
	Overloaded bool `json:"overloaded"`
	// For some protocols (like hopping) port is undefined.
	Port int `json:"port"`
	// TCP, UDP or KCP. This was called "protocol" before.
	Transport string `json:"transport"`
	// Type of bridge.
	Type string `json:"type"`
	// Options contain the map of options that will be passed to the client. It usually
	// contains authentication credentials.
	Options map[string]any `json:"options"`
	// Bucket is a "bucket" tag that connotes a resource group that a user may or may not
	// have access to. An empty bucket string implies that it is open access
	Bucket string `json:"bucket"`
	// LastSeenMillis is a unix time in milliseconds representing the last time we received a heartbeat update from this bridge
	LastSeenMillis int64 `json:"last_seen_millis" format:"int64"`
}

// Provider contains required information to configure and bootstrap the provider's service.
type Provider struct {
	// URL of the API endpoints
	ApiUri string `json:"api_uri"`
	// oldest supported api version
	// deprecated: kept for backwards compatibility. Replaced by api_versions.
	ApiVersion string `json:"api_version"`
	// all API versions the provider supports
	ApiVersions []string `json:"api_versions"`
	// Flag indicating whether to show regularly a donation reminder
	AskForDonations bool `json:"ask_for_donations"`
	// fingerprint of CA cert used to setup TLS sessions during VPN setup (and up to API version 3 for API communication)
	// deprecated: kept for backwards compatibility
	CaCertFingerprint string `json:"ca_cert_fingerprint"`
	// URL to fetch the CA cert used to setup TLS sessions during VPN setup (and up to API version 3 for API communication)
	// deprecated: kept for backwards compatibility
	CaCertUri string `json:"ca_cert_uri"`
	// Default language this provider uses to show infos and provider messages
	DefaultLanguage string `json:"default_language"`
	// Short description about the provider
	Description map[string]string `json:"description"`
	// Domain of the provider
	Domain string `json:"domain"`
	// URL to the donation website
	DonateURL string `json:"donate_url,omitempty"`
	// Number of days until a donation reminder reappears
	DonatePeriod string `json:"donate_period"`
	// URL to general provider website
	InfoURL string `json:"info_url,omitempty"`
	// Languages the provider supports to show infos and provider messages
	Languages []string `json:"languages"`
	// URL to the message of the day service
	MotdURL string `json:"motd_url,omitempty"`
	// Provider name
	Name map[string]string `json:"name"`
	// Operational properties which describe how the provider offers the service
	Service struct {
		// Flag indicating if anonymous usage without registration is allowed
		// deprecated: kept for backwards compatibility
		AllowAnonymous bool `json:"allow_anonymous"`
		// Flag indicating if the provider supports user registration
		// deprecated: kept for backwards compatibility
		AllowRegistration bool `json:"allow_registration"`
	} `json:"service"`
	// List of services the provider offers, currently only openvpn
	Services []string `json:"services"`
	//URL to Terms of Service website
	TosURL string `json:"tos_url,omitempty"`
	// list of STUN servers (format: ip/hostname:port) servers to get current ip address
	// can consist of self hosted STUN servers, public ones or a combination of both.
	// GeolocationLookup is only done when the list of STUNServers is not empty
	STUNServers []string `json:"stun_servers,omitempty"`
	// URL of a service that returns a country code for an ip address. If empty,
	// OONI backend is used
	CountryCodeLookupURL string `json:"country_code_lookup_url,omitempty"`
}
