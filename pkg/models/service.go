package models

// EIPService describes the serialization of a service description format used by LEAP.
type EIPService struct {
	Serial        int                  `json:"serial"`
	Version       int                  `json:"version"`
	Auth          string               `json:"auth"`
	Locations     map[string]*Location `json:"locations"`
	OpenVPNConfig map[string]any       `json:"openvpn_configuration"`
}

// EIPServiceV3 describes the JSON serialization of the legacy EIP Service format (v3).
type EIPServiceV3 struct {
	Serial   int `json:"serial"`
	Version  int `json:"version"`
	Gateways []struct {
		Host         string `json:"host"`
		IPAddr       string `json:"ip_address"`
		Location     string `json:"location"`
		Bucket       string `json:"bucket"`
		Capabilities struct {
			Transport []struct {
				Type      string         `json:"type"`
				Protocols []string       `json:"protocols"`
				Ports     []string       `json:"ports"`
				Options   map[string]any `json:"options"`
			} `json:"transport"`
		} `json:"capabilities"`
	}
	Locations     map[string]map[string]string `json:"locations"`
	OpenVPNConfig map[string]any               `json:"openvpn_configuration"`
}
