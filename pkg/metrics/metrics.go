package main

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var HitsPerCountry = promauto.NewCounterVec(prometheus.CounterOpts{
	Name: "menshen_hits",
	Help: "Number of hits in the geolocation service",
},
	[]string{"country"},
)
