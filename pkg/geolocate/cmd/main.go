package main

import (
	"fmt"
	"strings"

	geo "0xacab.org/leap/menshen/pkg/geolocate"
	"0xacab.org/leap/menshen/pkg/models"
)

func main() {
	locations := []*models.Location{
		{Label: "nyc", Lat: "40.71", Lon: "-74.01"},
		{Label: "par", Lat: "48.85", Lon: "2.35"},
		{Label: "stl", Lat: "47.61", Lon: "-122.33"},
		{Label: "ams", Lat: "52.37", Lon: "4.90"},
		{Label: "mia", Lat: "25.77", Lon: "-80.19"},
		{Label: "mtl", Lat: "45.52", Lon: "-73.65"},
	}

	for _, cc := range geo.CountryCodes() {
		sorted := geo.SortLocationsByDistanceToCountry(cc, locations)
		gws := []string{}
		for _, g := range sorted {
			gws = append(gws, g.Label)
		}
		fmt.Printf("%s,%s\n", cc, strings.Join(gws, ":"))
	}
}
