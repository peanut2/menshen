package geolocate

import (
	"fmt"
	"math"
	"sort"
	"strconv"
	"strings"

	"github.com/apex/log"
	g "github.com/kellydunn/golang-geo"
	"golang.org/x/exp/slices"
	"gonum.org/v1/gonum/floats"

	"0xacab.org/leap/menshen/pkg/latency"
	"0xacab.org/leap/menshen/pkg/models"
)

// return at maximum this number of locations. As a rule, given the typical numbers, set this
// to the maximum number of locations in a single country.
const maxLocations int = 2

// PickBestLocations calls the two methods (distance, latency) and merges the results.
// Note: this method picks the union of the two calls, so it might actually return more than `maxLocations`.
// TODO: this method is a workaround to the fact that the latency calculation is very blurry since it
// takes the average of the whole country.
// TODO: this is a good place to add region preferences.
func PickBestLocations(lm *latency.Metric, cc string, locations []*models.Location) []*models.Location {
	var err error
	if lm == nil {
		lm, err = latency.NewMetric()
		if err != nil {
			return nil
		}
	}

	loc := locations[:]
	set := make(map[string]*models.Location)

	byDist := SortLocationsByDistanceToCountry(cc, loc)
	byDist = byDist[:min(len(byDist), maxLocations)]
	byLat := SortLocationsByEstimatedLatencyToCountry(lm, cc, loc)
	byLat = byLat[:min(len(byLat), maxLocations)]

	for _, el := range byDist {
		fmt.Println(">>> by distance", el.Label)
		set[el.Label] = el
	}

	for _, el := range byLat {
		fmt.Println(">>> by latency", el.Label)
		set[el.Label] = el
	}

	selected := []*models.Location{}

	for _, _loc := range set {
		if slices.Contains(byDist, _loc) && slices.Contains(byLat, _loc) {
			selected = append(selected, _loc)
		}
	}
	if len(selected) <= maxLocations {
		return selected
	}

	// another pass, prio?
	return selected[:maxLocations]

}

// SortLocationsByDistanceToCountry return an array of locations sorted by "great circle distance"
// to the centroid of a given country. This is a very naive approach,
// but it's probably good enough for a small number of hand-picked locations like the Riseup
// provider uses right now.
//
// You might also be interested in using SortLocationsByEstimatedLatency (see below), which uses
// an empirical model of a latency metric.
//
// For the sake of fairness, and for providers with limited resources in terms
// of adding different locations, we might assign gateways to specific regions. The region and continent
// fields might be useful for these lookups.
func SortLocationsByDistanceToCountry(cc string, locations []*models.Location) []*models.Location {
	loc := locations[:]
	sort.Slice(loc, func(i, j int) bool {
		pf := strconv.ParseFloat

		pilat, _ := pf(locations[i].Lat, 64)
		pilon, _ := pf(locations[i].Lon, 64)
		pjlat, _ := pf(locations[j].Lat, 64)
		pjlon, _ := pf(locations[j].Lon, 64)

		pi := &Point{pilat, pilon}
		pj := &Point{pjlat, pjlon}

		di, _ := DistanceFromCountry(cc, pi)
		dj, _ := DistanceFromCountry(cc, pj)

		return di < dj
	})
	if len(loc) <= maxLocations {
		return loc
	} else {
		return loc[:3]
	}
}

// TODO: work in progress. This is correctly sorting locations by latency to the AVERAGE latency
// to the country in which the location is in, but we might need to desaggregate the database.
// Problem is that we need the equivalent of cloud regions: sort by proximity to US-west, AS-east etc.
func SortLocationsByEstimatedLatencyToCountry(
	lm *latency.Metric,
	cc string,
	locations []*models.Location) []*models.Location {
	var err error
	if lm == nil {
		lm, err = latency.NewMetric()
		if err != nil {
			return nil
		}
	}

	loc := locations[:]
	sort.Slice(loc, func(i, j int) bool {
		distance := GetEstimatedLatencyBetweenCountries
		di := distance(lm, cc, loc[i].CountryCode)
		dj := distance(lm, cc, loc[j].CountryCode)
		return di < dj

	})
	if len(loc) <= maxLocations {
		return loc
	} else {
		return loc[:3]
	}
}

// DistanceFromCountry returns the great circle distance between a target
// `Point` and a country (specified by country code).
func DistanceFromCountry(cc string, to *Point) (float64, error) {
	cc = strings.ToUpper(cc)
	p, ok := centroids[cc]
	if !ok {
		return math.Inf(1), fmt.Errorf("%s: %s", ErrUnknownCountry, cc)
	}
	return Distance(p, to), nil
}

// DistanceBetweenCountries expects two country codes, and it will return the
// great circle distance (in km) between the centroids for the two countries.
func DistanceBetweenCountries(cc string, to string) (float64, error) {
	to = strings.ToUpper(to)
	p, ok := centroids[to]
	if !ok {
		return math.Inf(1), fmt.Errorf("%s: %s", ErrUnknownCountry, to)
	}
	return DistanceFromCountry(cc, p)

}

// GetNearestCountry receives an array of country codes, and returns the one that is nearest.
func GetNearestCountry(from string, countryCodes []string) (string, error) {
	dist := []float64{}
	for _, cc := range countryCodes {
		d, err := DistanceBetweenCountries(from, cc)
		if err != nil {
			return "", err
		}
		dist = append(dist, d)

	}

	// calculate index for the distance array
	idx := slices.Index(dist, floats.Min(dist))
	return countryCodes[idx], nil

}

// Distance returns the great circle distance between two `Points`.
func Distance(from, to *Point) float64 {
	p := g.NewPoint(from.Lat, from.Lon)
	p2 := g.NewPoint(to.Lat, to.Lon)
	return p.GreatCircleDistance(p2)
}

// GetEstimatedLatencyBetweenCountries will return an estimation of latency (in ms) derived
// from an empirical dataset. This can be used to correct naive distance calculations, and sort
// locations accordingly.
func GetEstimatedLatencyBetweenCountries(lm *latency.Metric, from, to string) float64 {
	// First, we check if the latency metric knows about our origin country.
	// watch out: we're assuming that the *destination* is always in the latency
	// database (because of the bias in the dataset).
	// This assumption might break, so YMMV.
	from = strings.ToUpper(from)
	countriesWithData := lm.Keys()
	if slices.Contains(countriesWithData, from) {
		// just return the average latency.
		return lm.Distance(from, to)
	}

	// Country not found in the latency matrix. No problem, we only have to find which one is the
	// nearest country to our origin.
	nearest, err := GetNearestCountry(from, countriesWithData)
	if err != nil {
		return math.Inf(1)
	}

	//log.Debugf("nearest is: %s", nearest)
	fmt.Printf("nearest is: %s\n", nearest)

	// The long leg is a known latency metric (ms)
	long := lm.Distance(to, nearest)

	// the short leg is distance (in kilometers)
	short, err := DistanceBetweenCountries(from, nearest)
	if err != nil {
		return math.Inf(1)
	}

	log.Debugf("short is: %.3f km apart", short)

	// ... so we just have to add a correction that takes the theoretical speed of light into account.
	return long + getSpeedOfLightDelay(short)
}

const c float64 = 299792458e6

// getSpeedOfLightDelay calculates the correction for the theorical speed of light transmission.
// This magnitude will very likely always be negligible. For reference, this will sum ~30ms for 10_000 km.
func getSpeedOfLightDelay(km float64) float64 {
	return km / c
}
