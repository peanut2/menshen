package geolocate

import (
	"fmt"
	"strings"
)

var ErrUnknownCountry = "unknown country code"

// GetCentroidForCountry returns a *Point for the given country, and an error
// if the lookup was not successful.
func GetCentroidForCountry(cc string) (*Point, error) {
	val, ok := centroids[cc]
	if !ok {
		return nil, fmt.Errorf("%s: %s", ErrUnknownCountry, cc)
	}
	return val, nil
}

func CountryCodes() []string {
	keys := make([]string, 0, len(centroids))
	for k := range centroids {
		keys = append(keys, k)
	}
	return keys
}

func IsValidCountryCode(cc string) bool {
	for _, val := range CountryCodes() {
		if cc == val {
			return true
		}
	}
	return false
}

// Continent manipulation

const (
	XX = iota // unknown
	AN        // antarctica
	AS        // asia
	AF        // africa
	EU        // europe
	NA        // north america
	SA        // south america
	OC        // oceania
)

var continentCodeMap = map[int]string{
	XX: "XX",
	AN: "AN",
	AS: "AS",
	AF: "AF",
	EU: "EU",
	NA: "NA",
	SA: "SA",
	OC: "OC",
}

var continentNameMap = map[string]int{
	"unknown_continent": XX,
	"antarctica":        AN,
	"asia":              AS,
	"africa":            AF,
	"north_america":     NA,
	"south_america":     SA,
	"oceania":           OC,
}

func ContinentNameToInt(s string) int {
	return continentNameMap[s]
}

func ContinentCodeToLabel(i int) string {
	return continentCodeMap[i]
}

func GetContinentForCountryCode(cc string) int {
	country := regions[strings.ToUpper(cc)]
	fmt.Println("cc", country)
	if country == nil || country.Continent == "" {
		return XX
	}
	return continentNameMap[country.Continent]
}

// Region manipulation

const (
	UnknownRegion = iota
	North
	South
)

var regionMap = map[string]int{
	"global_north": North,
	"global_south": South,
}

func GetRegionForCountryCode(cc string) int {
	country := regions[strings.ToUpper(cc)]
	if country == nil || country.Region == "" {
		return UnknownRegion
	}
	return regionMap[country.Region]
}

func RegionToLabel(i int) string {
	switch i {
	case North:
		return "N"
	case South:
		return "S"
	default:
		return "X"
	}
}

func RegionFromLabel(s string) int {
	switch s {
	case "N":
		return North
	case "S":
		return South
	default:
		return UnknownRegion
	}
}
