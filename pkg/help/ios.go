package help

var bird = "\n" +
	"\n" +
	"                 all canaries are beautiful!\n" +
	"               ------------------------------\n" +
	"                                  /\n" +
	"                 _.----._        /\n" +
	"               ,'.::.--..:._\n" +
	"              /::/_,-(o)::;_`-._\n" +
	"             ::::::::`-';'`,--`-`\n" +
	"             ;::;'|::::,','\n" +
	"           ,'::/  ;:::/, :.\n" +
	"          /,':/  /::;' \\ ':\\\n" +
	"         :'.:: ,-''   . `.::\\\n" +
	"         \\.:;':.    `    :: .:\n" +
	"         (;' ;;;       .::' :|\n" +
	"          \\,:;;      \\ `::.\\.\n" +
	"          `);'        '::'  `:\n" +
	"           \\.  `        `'  .:      _,'\n" +
	"            `.: ..  -. ' :. :/  _.-' _.-\n" +
	"              \\;._.:._.;,-=_(.-'  __ `._\n" +
	"            ,;'  _..-((((''  .,-''  `-._\n" +
	"         _,'<.-''  _..``'.'`-'`.        `\n" +
	"     _.-((((_..--''       \\ \\ `.`.\n" +
	"   -'  _.``'               \\      ` fts\n" +
	"     ,'\n"

var HelpiOS = `<!DOCTYPE html><html><head><meta
charset="utf-8"><meta
name="viewport" content="width=device-width, initial-scale=1">
<title>RiseupVPN for iOS Howto</title>
<style type="text/css">
body{ margin:40px auto; max-width:650px; line-height:1.6; font-size:16px; color:#444; padding:0 10px}
h1,h2,h3{line-height:1.2}
pre{font-size:12px}
</style></head>
<body>
<h1>Howto: RiseupVPN for iOS</h1>
<pre>` + bird + `
</pre>

<p>RiseupVPN does not have an official iOS client yet.</p>

<p>But now you can use the official OpenVPN connect app, and import a riseupvpn
profile. It is a somewhat more manual process (and you will have to repeat
it every time the certificates expires, once every month or so), but hey, 
it might be better than nothing.</p>

Here's how:


<ol>
<li>
Install <strong>OpenVPN Connect</strong> from the <a
    href="https://apps.apple.com/us/app/openvpn-connect-openvpn-app/id590379981">App
    Store</a>. This is the official app from the OpenVPN developers.</li>
 
<li><p>Open a browser and navigate to <a href="https://menshen.asdf.network/autoconf">
        https://menshen.asdf.network/autoconf</a> (*)</p>
    <p>This will prompt you to download a file called "riseup.ovpn". We do want, so click "Download".</p></li>
   
<li><p>Open the Files app, and <strong>long-press</strong> on the file you just downloaded (riseup.ovpn).</p></li>
<li><p>Click "<strong>Share</strong>".</p></li>
<li><p>Share to the OpenVPN app.</p></li>
<li><p>The OpenVPN Connect app should open in the "<strong>Import Profile</strong>" pane, under the "File" tab. Click "Add".</p></li>
<li><p>You can change the "Profile Name" if you want (maybe you want to remove the reference to riseup and put your $boringjob or $uni here).</p></li>
<li><p>Click "Connect"</p></li>
<li><p>If everything went well, you should be connected to the new profile!</p></li>
</ol>


That's all!

And, if you can afford it, please remember to make <a href="https://riseup.net/vpn/donate">a donation!</a> 


<h2>Notes</h2>

[*] This is an unnoficial testing site maintained by some friendly birds. If you're reading this, you already know us and want to help testing this... <pre>;)</pre>
</body>
</html>
`
