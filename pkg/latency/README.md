# latency experiments

have a look at a [brief latency study](https://gitlab.com/kalikaneko/latency/) I did while trying to answer good heuristics to distribute gateways according to a more realistic model of the internet.

TL;DR: great circle calculations are useless for the typical size and gateway pools of the providers we're interested in.

The topic complicates further if we want to introduce any notion of fairness allocation that takes the digital divide into account. I suspect this is what it was behind the "hemisphere" tagging in the legacy bonafide, something that has never been used in prractice.

## lessons learned

* empirical measurement of latency goes a long way (i.e., ping the less congested gateway on each region, use that).
* consider developing a "**preferential attachment**" model. E.g., mark resources with a broad region tag, and make the probability of them being assigned to a client *claiming to be* outside of the region (i.e., assume a well-behaved client) as inversely proportional to the complement of the fractional load.


