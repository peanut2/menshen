package latency

import (
	"embed"
	"encoding/csv"
	"log"
	"math"
	"strconv"

	"golang.org/x/exp/slices"
	"gonum.org/v1/gonum/mat"
)

//go:embed distance.csv
var f embed.FS

type Metric struct {
	keys   []string
	matrix *mat.Dense
}

func NewMetric() (*Metric, error) {
	// Open the embedded latency source file
	file, err := f.Open("distance.csv")
	if err != nil {
		log.Println("Error:", err)
		return nil, err
	}
	defer file.Close()

	// Create a new reader.
	reader := csv.NewReader(file)

	// Read all the records
	records, err := reader.ReadAll()
	if err != nil {
		log.Println("Error:", err)
		return nil, err
	}

	keys := []string{}

	for i, record := range records {
		if i == 0 {
			continue
		}
		from := record[1]

		if !slices.Contains(keys, from) {
			keys = append(keys, from)
		}
	}

	// create matrix
	n := len(keys)
	matrix := mat.NewDense(n, n, nil)

	for _, record := range records {
		from := record[1]
		to := record[2]

		val, err := strconv.ParseFloat(record[3], 64)
		if err != nil {
			continue
		}

		m := slices.Index(keys, from)
		n := slices.Index(keys, to)
		matrix.Set(m, n, val)

	}

	l := &Metric{
		keys:   keys,
		matrix: matrix,
	}
	return l, nil
}

// Len returns the number of keys known to this object
func (m *Metric) Len() int {
	if m.keys == nil {
		return 0
	}
	return len(m.keys)
}

// Keys return all the known keys in this matrix (list of ISO-2 country codes)
func (m *Metric) Keys() []string {
	return m.keys
}

// Distance returns the average latency (measured in ms) between to given country codes.
func (m *Metric) Distance(from, to string) float64 {
	if !slices.Contains(m.keys, from) || !slices.Contains(m.keys, to) {
		return math.Inf(1)
	}
	j := slices.Index(m.keys, from)
	k := slices.Index(m.keys, to)
	return m.matrix.At(j, k)
}
