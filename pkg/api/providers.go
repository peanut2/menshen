package api

import (
	"crypto/tls"
	"crypto/x509"
	"net/http"
)

// initTLSProxy initializes an http Client that has any needed pinned certificates
// for TLS verification.
func initTLSProxy(ca []byte) http.Client {
	certs := x509.NewCertPool()
	certs.AppendCertsFromPEM(ca)
	tlsConfig := &tls.Config{
		RootCAs: certs,
	}
	t := &http.Transport{
		TLSClientConfig: tlsConfig,
	}
	return http.Client{
		Transport: t,
	}
}
