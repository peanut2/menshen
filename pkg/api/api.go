package api

import (
	"fmt"
	"net/http"

	"github.com/apex/log"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	echoSwagger "github.com/swaggo/echo-swagger"
	"golang.org/x/crypto/acme/autocert"

	sw "0xacab.org/leap/menshen/api"
	"0xacab.org/leap/menshen/storage"

	"0xacab.org/leap/menshen/pkg/help"
	"0xacab.org/leap/menshen/pkg/loadbalancer"
)

func InitMetricsServer() *echo.Echo {
	e := echo.New()
	e.GET("/metrics", echo.WrapHandler(promhttp.Handler()))
	return e
}

func InitServer(cfg *Config) *echo.Echo {
	log.Info("Initializing server...")

	if cfg.AutoTLS {
		sw.SwaggerInfo.Host = cfg.ServerName
	} else {
		sw.SwaggerInfo.Host = fmt.Sprintf("localhost:%d", cfg.Port)
	}

	e := echo.New()

	if cfg.AutoTLS {
		e.AutoTLSManager.HostPolicy = autocert.HostWhitelist(cfg.ServerName)
		// Cache certificates to avoid issues with rate limits (https://letsencrypt.org/docs/rate-limits)
		e.AutoTLSManager.Cache = autocert.DirCache("/var/www/.cache")
	}

	db, err := storage.OpenDatabase(cfg.DBFile)
	if err != nil {
		log.Fatalf("Error opening database: %v", err)
	}

	e.Use(storageMiddleware(db))

	e.Use(authTokenMiddleware)

	// We don't want no panics in production
	//e.Use(middleware.Recover())
	e.Use(middleware.Logger())

	if cfg.HasLegacyEIPFile() {
		log.Info("Loading EIP file")
	}

	r, err := newRegistry(cfg)
	if err != nil {
		log.Fatal(err.Error())
	}

	log.Infof("Starting load balancer on: %s", cfg.LoadBalancerAddr)
	lb, err := loadbalancer.StartLoadBalancer(cfg.LoadBalancerAddr)
	if err != nil {
		log.Fatal(err.Error())
	}
	r.lb = lb

	// Enabled with flag --enable-cert-v3 or env ENABLE_CERT_V3=True
	if cfg.EnableCertv3 {
		e.GET("/3/cert", CertGenHelper(r, cfg.OvpnCaCrt, cfg.OvpnCaKey, "rsa", cfg.OvpnClientCrtExpiry, false))
	}
	// TODO this is a simple API to serve a single randomized gateway per location
	e.GET("/api/5/gateway", r.GatewayPicker)
	e.GET("/api/5/bridge/:location", r.BridgePicker)

	if cfg.AllowGatewayListing {
		e.GET("/api/5/gateways", r.ListAllGateways)
	}
	if cfg.AllowBridgeListing {
		e.GET("/api/5/bridges", r.ListAllBridges)
	}

	e.GET("/api/5/service", r.ServiceInfo)

	e.GET("/api/5/openvpn/cert", CertGenHelper(r, cfg.OvpnCaCrt, cfg.OvpnCaKey, cfg.Algo, cfg.OvpnClientCrtExpiry, true))

	e.GET("/api/5/openvpn/config", GenConfigHelper(r, cfg))

	e.GET("/provider.json", r.GetProvider)

	// document the API serving the Swagger spec
	e.GET("/api/swagger/*", echoSwagger.WrapHandler)

	e.GET("/api/autoconf", DownloadConfigHelper(r, cfg))

	e.GET("/api/help/ios", func(c echo.Context) error {
		return c.HTML(http.StatusOK, help.HelpiOS)
	})

	agentEndpoints := e.Group("/api/5/agent")
	agentEndpoints.Use(agentRegistrationMiddleware(cfg.AgentSharedKey))
	// Limit agent registration requests to 10MB
	agentEndpoints.Use(middleware.BodyLimit("10M"))
	agentEndpoints.PUT("/bridge", r.RegisterBridge)
	agentEndpoints.PUT("/gateway", r.RegisterGateway)

	e.HideBanner = true
	return e
}
