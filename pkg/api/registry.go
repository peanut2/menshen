package api

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"0xacab.org/leap/menshen/pkg/geolocate"
	"0xacab.org/leap/menshen/pkg/latency"
	"0xacab.org/leap/menshen/pkg/loadbalancer"
	m "0xacab.org/leap/menshen/pkg/models"
)

type gatewayMap map[string][]*m.Gateway
type bridgeMap map[string][]*m.Bridge
type locationMap map[string]*m.Location

type transportType string

var (
	transportOpenVPN transportType = "openvpn"
	transportOBFS4   transportType = "obfs4"

	tmpEIPFilePath = "/tmp/leap-eip-legacy.json"
)

type registry struct {
	gateways      gatewayMap
	bridges       bridgeMap
	locations     locationMap
	openvpnConfig map[string]any

	// client is the http.Client that will be used for all the underlying operations
	client http.Client

	// lb is a pointer to a LoadBalancer instance.
	lb *loadbalancer.LoadBalancer

	// lm is a latency metric instance.
	lm *latency.Metric

	clientCertURL string
	// ca is the pem formatted root ca
	ca       string
	provider m.Provider

	// This is the amount of milliseconds to wait since the last heartbeat from a bridge or gateway before
	// removing them from the resources that are returned to clients.
	// If this is zero then no cutoff will be applied
	lastSeenCutoffMillis int64
}

func newRegistry(cfg *Config) (*registry, error) {
	gateways := make(map[string][]*m.Gateway, 0)
	bridges := make(map[string][]*m.Bridge, 0)
	openvpnConfig := make(map[string]any, 0)
	locations := make(locationMap, 0)

	if cfg.ProviderJson == "" {
		log.Fatal("File path to provider.json is missing.")
	}
	provider, err := ParseProviderJsonFile(cfg.ProviderJson)
	if err != nil {
		return nil, fmt.Errorf("failed to parse provider.json: %v", err)
	}

	if cfg.CaFile == "" {
		log.Fatal("File path to root CA is missing.")
	}
	var httpClient http.Client
	ca, err := os.ReadFile(cfg.CaFile)
	if err != nil {
		log.Fatalf(fmt.Sprintf("Could not read CA file '%s': %s", cfg.CaFile, err))
	}
	httpClient = initTLSProxy(ca)

	if cfg.HasLegacyEIPFile() {
		// As a intermediate step, we can configure menshen with the legacy
		// eip-service JSON format served as of version 3.
		// TODO: this should be moved into its own parsing function.
		if cfg.EIPURL != "" && cfg.EIP != "" {
			log.Fatal("Both eip-url and eip-file cannot be set at the same time.")
		}
		var eipFile string

		// We give priority to a local file.
		if cfg.EIP != "" {
			eipFile = cfg.EIP
		} else {
			// It must be that we have a URL, because HasLegacyEIPFile returns either or.
			if err := downloadToFile(httpClient, cfg.EIPURL, tmpEIPFilePath); err != nil {
				log.Fatalf("Could not fetch external EIP File: %s", err)
			}
			eipFile = tmpEIPFilePath
			defer os.Remove(tmpEIPFilePath)
		}
		eip, err := ParseEIPServiceFile(eipFile)
		if err != nil {
			return nil, err
		}
		locations = GetLocationsFromEIPData(eip)
		locations = maybeAddGeolocation(locations)

		for _, g := range eip.Gateways {
			for _, transport := range g.Capabilities.Transport {
				switch transport.Type {
				case string(transportOpenVPN):
					// Transport "openvpn" is what we consider a standard gateway.
					for _, proto := range transport.Protocols {
						for _, port := range transport.Ports {
							p, err := strconv.Atoi(port)
							if err != nil {
								continue
							}
							loc := m.CanonicalizeLocation(g.Location)
							newGateway := &m.Gateway{
								Healthy:        true,
								Host:           g.Host,
								IPAddr:         g.IPAddr,
								Location:       loc,
								Bucket:         g.Bucket,
								Port:           p,
								Transport:      proto,
								Type:           string(transportOpenVPN),
								LastSeenMillis: time.Now().UnixMilli(),
							}
							_, exists := gateways[loc]
							if !exists {
								// key not already in the location map
								gateways[loc] = []*m.Gateway{newGateway}
							} else {
								gateways[loc] = append(gateways[loc], newGateway)
							}
						}
					}
				case string(transportOBFS4):
					for _, proto := range transport.Protocols {
						for _, port := range transport.Ports {
							p, err := strconv.Atoi(port)
							if err != nil {
								continue
							}
							loc := m.CanonicalizeLocation(g.Location)
							bridge := &m.Bridge{
								Healthy:        true,
								Type:           string(transportOBFS4),
								Host:           g.Host,
								Location:       loc,
								Bucket:         g.Bucket,
								IPAddr:         g.IPAddr,
								Port:           p,
								Transport:      proto,
								Options:        transport.Options,
								LastSeenMillis: time.Now().UnixMilli(),
							}
							_, exists := bridges[loc]
							if !exists {
								// key not already in the location map
								bridges[loc] = []*m.Bridge{bridge}
							} else {
								bridges[loc] = append(bridges[loc], bridge)
							}
							if _, exists := locations[loc]; exists {
								locations[loc].HasBridges = true
							} else {
								log.Fatalf("Could not find matching loation %s in locations list", loc)
							}
						}
					}

				default:
				}
			}
		}
		for k, v := range eip.OpenVPNConfig {
			// TODO: validate against a list of allowed keys
			switch v := v.(type) {
			case string:
				openvpnConfig[k] = v
			case bool:
				openvpnConfig[k] = v
			default:
			}
		}
	}

	// TODO should use a better mechanism here - can we combine with the load balancer gRPC?
	// TODO move to a different map (for private bridges).

	time.Sleep(time.Second) // just to make sure all nodes are up.
	log.Printf("Bridges: %v", cfg.LocalBridges)

	for _, controlBridge := range cfg.LocalBridges {
		_parts := strings.Split(controlBridge, ":")
		host := _parts[0]

		url := fmt.Sprintf("http://%s/bridge", controlBridge)
		log.Printf("Fetching bridge info from %s", url)
		resp, err := http.Get(url)
		if err != nil {
			log.Printf("error: %v", err)
			continue
		}
		defer resp.Body.Close()
		if resp.StatusCode != http.StatusOK {
			log.Printf("status code: %v", resp.StatusCode)
			continue
		}
		body, err := io.ReadAll(resp.Body)
		if err != nil {
			log.Printf("error reading: %v", err)
			continue
		}
		var bridge m.Bridge
		if err := json.Unmarshal(body, &bridge); err != nil {
			log.Printf("error unmarshal: %v", err)
		}
		bridge.Host = host
		if bridge.Type == "obfs4" {
			bridge.Transport = "tcp"
		}
		bridges[bridge.Location] = []*m.Bridge{&bridge}
		log.Println("Parsed bridge status ok")

	}

	log.Printf("%d locations", len(locations))
	log.Printf("%d locations with gateways\n", len(gateways))
	log.Printf("%d locations with bridges\n", len(bridges))
	log.Println("openvpn_config:", openvpnConfig)

	lm, err := latency.NewMetric()
	if err != nil {
		log.Println("Error initializing latency metric", err)
	}

	r := &registry{
		gateways:             gateways,
		bridges:              bridges,
		locations:            locations,
		openvpnConfig:        openvpnConfig,
		lm:                   lm,
		clientCertURL:        cfg.ClientCertURL,
		ca:                   string(ca),
		provider:             *provider,
		lastSeenCutoffMillis: cfg.LastSeenCutoffMillis,
	}
	r.client = httpClient
	return r, nil
}

func (r *registry) Stop() {
	r.lb.Stop()
}

func (r *registry) AllGateways() []*m.Gateway {
	gateways := []*m.Gateway{}
	for k := range r.gateways {
		gateways = append(gateways, r.gateways[k]...)
	}
	return gateways
}

func (r *registry) AllLocations() []*m.Location {
	locations := []*m.Location{}
	for k := range r.locations {
		locations = append(locations, r.locations[k])
	}
	return locations
}

func GetLocationsFromEIPData(eip *m.EIPServiceV3) locationMap {
	locations := make(locationMap, 0)
	for locName, location := range eip.Locations {
		label := m.CanonicalizeLocation(locName)
		newLoc := &m.Location{
			// Defaulting to true. This still needs to be computed -
			// probably allowing for overriding via a maintenance mode,
			// or monitoring infra.
			Healthy:     true,
			Label:       label,
			DisplayName: location["name"],
			CountryCode: location["country_code"],
			Hemisphere:  location["hemisphere"],
			Timezone:    location["timezone"],
			Lat:         location["lat"],
			Lon:         location["lon"],
		}
		locations[label] = newLoc
	}
	return locations
}

// maybeAddGeolocation gets a map[string]*models.Location and it adds geolocation
// data from an offline library. It is expected that the provider manually adds the precise
// geolocation, but this is useful for providers that have not migrated from EIPServiceV3.
func maybeAddGeolocation(locations locationMap) locationMap {
	targets := make([]string, 0)
	for loc := range locations {
		targets = append(targets, loc)
	}
	coords := geolocate.GeolocateCities(targets)

	for name, loc := range locations {
		coord := coords[m.CanonicalizeLocation(name)]
		loc.Lat = fmt.Sprintf("%.2f", coord.Lat)
		loc.Lon = fmt.Sprintf("%.2f", coord.Lon)
	}
	return locations
}

func ParseProviderJsonFile(path string) (*m.Provider, error) {
	var provider m.Provider
	raw, err := os.ReadFile(path)
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(raw, &provider)
	if err != nil {
		return nil, err
	}
	return &provider, nil
}

func ParseEIPServiceFile(path string) (*m.EIPServiceV3, error) {
	var eip m.EIPServiceV3
	raw, err := os.ReadFile(path)
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(raw, &eip)
	if err != nil {
		return nil, err
	}
	return &eip, nil
}

func downloadToFile(client http.Client, url, fileName string) error {
	// Create blank file
	file, err := os.Create(fileName)
	if err != nil {
		return err
	}

	client.CheckRedirect = func(r *http.Request, via []*http.Request) error {
		r.URL.Opaque = r.URL.Path
		return nil
	}

	resp, err := client.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// Write content to file
	_, err = io.Copy(file, resp.Body)
	if err != nil {
		return err
	}
	defer file.Close()
	return nil
}
