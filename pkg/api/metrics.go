package api

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

func doIncrementCountryHit(cc string) {
	hitsPerCountry.With(prometheus.Labels{"country": cc}).Inc()
}

// TODO: ensure we're using them.

// hitsPerCountry declares a global vector counter for the hits that demand
// gateways for a given country.
var hitsPerCountry = promauto.NewCounterVec(prometheus.CounterOpts{
	Name: "menshen_hits",
	Help: "Number of hits in the geolocation service, by country",
},
	[]string{"country"},
)
