package api

import (
	"encoding/json"
	"net/http"
	"time"

	"0xacab.org/leap/menshen/pkg/models"
	"github.com/apex/log"
	"github.com/labstack/echo/v4"
)

func bridgesMatch(bridge1, bridge2 models.Bridge) bool {

	// bridge1 has options so we need to compare them
	if len(bridge1.Options) > 0 {
		bridge1OptionsBytes, err := json.Marshal(bridge1.Options)
		if err != nil {
			log.Errorf("Could not marshal bridge1 options. Bridge1: %#v, err: %v", err)
			return false
		}
		bridge2OptionsBytes, err := json.Marshal(bridge2.Options)
		if err != nil {
			log.Errorf("Could not marshal bridge2 options. Bridge2: %#v, err: %v", err)
			return false
		}

		if string(bridge1OptionsBytes) != string(bridge2OptionsBytes) {
			return false
		}
	}

	return bridge1.Auth == bridge2.Auth &&
		bridge1.Bucket == bridge2.Bucket &&
		bridge1.Experimental == bridge2.Experimental &&
		bridge1.Host == bridge2.Host &&
		bridge1.IP6Addr == bridge2.IP6Addr &&
		bridge1.IPAddr == bridge2.IPAddr &&
		bridge1.Location == bridge2.Location &&
		bridge1.Port == bridge2.Port &&
		bridge1.Transport == bridge2.Transport &&
		bridge1.Type == bridge2.Type
}

func gatewaysMatch(gateway1, gateway2 models.Gateway) bool {
	return gateway1.Bucket == gateway2.Bucket &&
		gateway1.Experimental == gateway2.Experimental &&
		gateway1.Host == gateway2.Host &&
		gateway1.IP6Addr == gateway2.IP6Addr &&
		gateway1.IPAddr == gateway2.IPAddr &&
		gateway1.Location == gateway2.Location &&
		gateway1.Port == gateway2.Port &&
		gateway1.Transport == gateway2.Transport &&
		gateway1.Type == gateway2.Type
}

// RegisterBridge godoc
// @Summary      Register a bridge
// @Description  Register a bridge. This endpoint allows "menshen agent" processes running on bridges to register themselves.
// @Tags         Agent
// @Accept       json
// @Produce      json
// @Param        bridge	 body  models.Bridge true "bridge"
// @Success      200  {object}  models.Bridge
// @Failure      400  {object}  error
// @Failure      404  {object}  error
// @Failure      500  {object}  error
// @Router       /api/5/agent/bridge [PUT]
// @Security AgentRegistrationAuth
func (r *registry) RegisterBridge(c echo.Context) error {
	var bridgeRequest models.Bridge

	// TODO: consider validation?
	err := c.Bind(&bridgeRequest)
	if err != nil {
		log.Errorf("failed to bind request body: %v", err)
		return c.JSON(http.StatusBadRequest, "bad request")
	}

	location := bridgeRequest.Location

	lastSeenTime := time.Now().UnixMilli()

	bridgeRequest.LastSeenMillis = lastSeenTime

	bridgesToUpdate := r.bridges[location]

	statusToReturn := http.StatusCreated
	found := false
	for i, bridge := range bridgesToUpdate {
		if bridgesMatch(*bridge, bridgeRequest) {
			// We only need to update the LastSeenMillis for the bridge we're "updating"
			bridgesToUpdate[i].LastSeenMillis = lastSeenTime
			found = true
			statusToReturn = http.StatusOK
			break
		}
	}

	if !found {
		bridgesToUpdate = append(bridgesToUpdate, &bridgeRequest)
	}

	r.bridges[location] = bridgesToUpdate

	return c.JSON(statusToReturn, nil)
}

// RegisterGateway godoc
// @Summary      Register a gateway
// @Description  Register a gateway. This endpoint allows "menshen agent" processes running on gateways to register themselves.
// @Tags         Agent
// @Accept       json
// @Produce      json
// @Param        gateway	 body  models.Gateway true "gateway"
// @Success      200  {object}  models.Gateway
// @Failure      400  {object}  error
// @Failure      404  {object}  error
// @Failure      500  {object}  error
// @Router       /api/5/agent/gateway [PUT]
// @Security AgentRegistrationAuth
func (r *registry) RegisterGateway(c echo.Context) error {
	var gatewayRequest models.Gateway

	// TODO: consider validation?
	err := c.Bind(&gatewayRequest)
	if err != nil {
		log.Errorf("failed to bind request body: %v", err)
		return c.JSON(http.StatusBadRequest, "bad request")
	}

	location := gatewayRequest.Location

	lastSeenTime := time.Now().UnixMilli()

	gatewayRequest.LastSeenMillis = lastSeenTime

	gatewaysToUpdate := r.gateways[location]

	statusToReturn := http.StatusCreated
	found := false
	for i, gateway := range gatewaysToUpdate {
		if gatewaysMatch(*gateway, gatewayRequest) {
			// We only need to update the LastSeenMillis for the gateway we're "updating"
			gatewaysToUpdate[i].LastSeenMillis = lastSeenTime
			found = true
			statusToReturn = http.StatusOK
			break
		}
	}

	if !found {
		gatewaysToUpdate = append(gatewaysToUpdate, &gatewayRequest)
	}

	r.gateways[location] = gatewaysToUpdate

	return c.JSON(statusToReturn, nil)
}
