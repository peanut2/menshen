package api

import (
	"crypto/ecdsa"
	"crypto/ed25519"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/rsa"
	"crypto/tls"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"fmt"
	"io"
	"log"
	"math/big"
	"net/http"
	"strings"
	"time"

	"github.com/labstack/echo/v4"
)

const certPrefix = "UNLIMITED"

// GenerateCert godoc
// @Summary      Get openvpn cert
// @Description  Fetch a new key and cert.
// @Tags         Provisioning
// @Accept       json
// @Produce      json
// @Success      200  {object}  string
// @Failure      400  {object}  error
// @Failure      404  {object}  error
// @Failure      500  {object}  error
// @Router       /api/5/openvpn/cert [get]
func CertGenHelper(registry *registry, ovpnCaCrt string, ovpnCaKey string, algo string, expiryDays int, addRootCa bool) func(echo.Context) error {
	return func(c echo.Context) error {
		// Set Cache-Control header to instruct clients not to cache response
		c.Response().Header().Set("Cache-Control", "no-store")
		if registry.clientCertURL == "" {
			crt, err := registry.CertWriter(ovpnCaCrt, ovpnCaKey, algo, expiryDays, addRootCa)
			if err != nil {
				return err
			}
			return c.String(http.StatusOK, crt)
		} else {
			return registry.CertProxy(c)
		}
	}
}

// CertWriter main handler
func (r *registry) CertWriter(ovpnCaCrt string, ovpnCaKey string, algo string, expiryDays int, addRootCa bool) (string, error) {
	addEnvelope := addRootCa
	catls, err := tls.LoadX509KeyPair(ovpnCaCrt, ovpnCaKey)
	if err != nil {
		return "", err
	}

	ca, err := x509.ParseCertificate(catls.Certificate[0])
	if err != nil {
		return "", err
	}

	serialNumberLimit := new(big.Int).Lsh(big.NewInt(1), 128)
	serialNumber, err := rand.Int(rand.Reader, serialNumberLimit)
	if err != nil {
		return "", err
	}

	subjectKeyID := make([]byte, 20)
	_, err = rand.Read(subjectKeyID)
	if err != nil {
		return "", err
	}

	// Prepare certificate
	cert := &x509.Certificate{
		SerialNumber: serialNumber,

		Subject: pkix.Name{
			CommonName: certPrefix,
		},
		NotBefore: time.Now().AddDate(0, 0, -1),
		NotAfter:  time.Now().AddDate(0, 0, expiryDays),

		SubjectKeyId: subjectKeyID,

		ExtKeyUsage: []x509.ExtKeyUsage{x509.ExtKeyUsageClientAuth},
		KeyUsage:    x509.KeyUsageDigitalSignature,
	}

	var buf strings.Builder
	if algo == "ecdsa" {
		priv, err := ecdsa.GenerateKey(elliptic.P521(), rand.Reader) // Use elliptic.P521() for ECDSA
		if err != nil {
			return "", err
		}
		pub := &priv.PublicKey

		// Sign the certificate
		certB, err := x509.CreateCertificate(rand.Reader, cert, ca, pub, catls.PrivateKey)
		if err != nil {
			return "", err
		}
		// Write the private key
		keyBytes, err := x509.MarshalPKCS8PrivateKey(priv)
		if err != nil {
			return "", err
		}
		if err = writePrivateKey(addEnvelope, false, &buf, keyBytes); err != nil {
			return "", err
		}

		// Write the public key
		if err = writeVpnCertificate(addEnvelope, &buf, certB); err != nil {
			return "", err
		}
	} else if algo == "ed25519" {
		pub, priv, err := ed25519.GenerateKey(rand.Reader)
		if err != nil {
			return "", err
		}
		// Sign the certificate
		certB, err := x509.CreateCertificate(rand.Reader, cert, ca, pub, catls.PrivateKey)
		if err != nil {
			return "", err
		}

		// Write the private Key
		privKeyBytes, err := x509.MarshalPKCS8PrivateKey(priv)
		if err != nil {
			return "", err
		}
		if err = writePrivateKey(addEnvelope, false, &buf, privKeyBytes); err != nil {
			return "", err
		}

		// Write the public key
		if err = writeVpnCertificate(addEnvelope, &buf, certB); err != nil {
			return "", err
		}

	} else if algo == "rsa" {
		// TODO: remove this once v3 is ready for full deprecation
		// this condition can be removed once we add eliptic curve support on clients
		keySize := 2048
		priv, err := rsa.GenerateKey(rand.Reader, keySize)
		if err != nil {
			return "", err
		}
		pub := &priv.PublicKey

		// Sign the certificate
		certB, err := x509.CreateCertificate(rand.Reader, cert, ca, pub, catls.PrivateKey)
		if err != nil {
			return "", err
		}

		// Write the private Key
		if err = writePrivateKey(addEnvelope, true, &buf, x509.MarshalPKCS1PrivateKey(priv)); err != nil {
			return "", err
		}

		// Write the public key
		if err = writeVpnCertificate(addEnvelope, &buf, certB); err != nil {
			return "", err
		}

	} else {
		return "", fmt.Errorf("specified certificate generation algorithm not supported: %v", algo)
	}

	if addRootCa {
		// Write the ca cert
		if err = writeCACertificate(addEnvelope, &buf, r.ca); err != nil {
			return "", err
		}
	}

	return buf.String(), nil
}

func maybeAddEnvelope(addEnvelope bool, buf io.Writer, tag string) error {
	if addEnvelope {
		_, err := buf.Write([]byte(fmt.Sprintf("%s\n", tag)))
		return err
	}
	return nil
}

func writePrivateKey(addEnvelope bool, isRsa bool, buf io.Writer, keyBytes []byte) error {
	if err := maybeAddEnvelope(addEnvelope, buf, "<key>"); err != nil {
		return err
	}
	if isRsa {
		if err := pem.Encode(buf, &pem.Block{Type: "RSA PRIVATE KEY", Bytes: keyBytes}); err != nil {
			return err
		}
	} else {
		if err := pem.Encode(buf, &pem.Block{Type: "PRIVATE KEY", Bytes: keyBytes}); err != nil {
			return err
		}
	}

	if err := maybeAddEnvelope(addEnvelope, buf, "</key>"); err != nil {
		return err
	}
	return nil
}

func writeVpnCertificate(addEnvelope bool, buf io.Writer, keyBytes []byte) error {
	if err := maybeAddEnvelope(addEnvelope, buf, "<cert>"); err != nil {
		return err
	}

	err := pem.Encode(buf, &pem.Block{Type: "CERTIFICATE", Bytes: keyBytes})
	if err != nil {
		return err
	}

	if err := maybeAddEnvelope(addEnvelope, buf, "</cert>"); err != nil {
		return err
	}
	return nil
}

func writeCACertificate(addEnvelope bool, buf io.Writer, pemRootCA string) error {
	if err := maybeAddEnvelope(addEnvelope, buf, "<ca>"); err != nil {
		return err
	}

	_, err := buf.Write([]byte(fmt.Sprintf("%s\n", pemRootCA)))
	if err != nil {
		return err
	}

	if err := maybeAddEnvelope(addEnvelope, buf, "</ca>"); err != nil {
		return err
	}
	return nil
}

func (r *registry) CertProxy(c echo.Context) error {
	req, err := http.NewRequest(http.MethodGet, r.clientCertURL, nil)
	if err != nil {
		log.Printf("client: could not create request: %s\n", err)
		return err
	}
	res, err := r.client.Do(req)
	if err != nil {
		return err
	}
	resBody, err := io.ReadAll(res.Body)
	if err != nil {
		log.Printf("client: could not read response body: %s\n", err)
		return err
	}
	// log.Printf("client: response body: %s\n", resBody)
	return c.String(http.StatusOK, string(resBody))
}
