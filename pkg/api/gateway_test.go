package api

import (
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"strings"
	"testing"
	"time"

	m "0xacab.org/leap/menshen/pkg/models"
	"0xacab.org/leap/menshen/storage"
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
)

func TestGatewayFilters(t *testing.T) {
	type authTokenDbEntry struct {
		key     string
		buckets string
	}

	locations := []string{"New York", "Paris", "Montreal"}
	countryCodes := []string{"US", "FR", "CA"}

	locationStruct1 := &m.Location{
		CountryCode: countryCodes[0],
		DisplayName: locations[0],
		Lat:         "40.71",
		Lon:         "74.00",
	}
	locationStruct2 := &m.Location{
		CountryCode: countryCodes[1],
		DisplayName: locations[1],
		Lat:         "46.22",
		Lon:         "2.21",
	}
	locationStruct3 := &m.Location{
		CountryCode: countryCodes[2],
		DisplayName: locations[2],
		Lat:         "45.50",
		Lon:         "73.56",
	}

	gateway1 := &m.Gateway{
		Location:       locations[0],
		Type:           "openvpn",
		Bucket:         "bucket1",
		LastSeenMillis: time.Now().UnixMilli(),
	}

	gateway2 := &m.Gateway{
		Location: locations[1],
		Type:     "openvpn",
	}

	gateway3 := &m.Gateway{
		Location: locations[2],
		Type:     "openvpn",
		Bucket:   "bucket2",
	}

	testTable := []struct {
		name         string
		mockRegistry *registry
		expected     func(*registry) string
		authToken    string
		dbAuthTokens []authTokenDbEntry
	}{
		{"no auth token only private gateways",
			&registry{
				gateways:  gatewayMap{locations[0]: []*m.Gateway{gateway1}, locations[2]: []*m.Gateway{gateway3}},
				locations: locationMap{locations[0]: locationStruct1, locations[1]: locationStruct2, locations[2]: locationStruct3},
			},
			func(r *registry) string {
				return "[]\n"
			},
			"",
			[]authTokenDbEntry{{"key1", "bucket1"}},
		},
		{"auth token one private gateway",
			&registry{
				gateways:  gatewayMap{locations[0]: []*m.Gateway{gateway1}, locations[2]: []*m.Gateway{gateway3}},
				locations: locationMap{locations[0]: locationStruct1},
			},
			func(r *registry) string {
				bytes, err := json.Marshal(r.gateways[locations[0]])
				assert.NoError(t, err)
				return string(bytes)
			},
			"key1",
			[]authTokenDbEntry{{"key1", "bucket1"}},
		},
		{"auth token one private one public gateway",
			&registry{
				gateways:  gatewayMap{locations[0]: []*m.Gateway{gateway1}, locations[1]: []*m.Gateway{gateway2}},
				locations: locationMap{locations[0]: locationStruct1},
			},
			func(r *registry) string {
				bytes, err := json.Marshal(r.gateways[locations[0]])
				assert.NoError(t, err)
				return string(bytes)
			},
			"key1",
			[]authTokenDbEntry{{"key1", "bucket1"}},
		},
		{"auth token with multiple buckets two private gateways",
			&registry{
				gateways:  gatewayMap{locations[0]: []*m.Gateway{gateway1}, locations[1]: []*m.Gateway{gateway3}},
				locations: locationMap{locations[0]: locationStruct1, locations[1]: locationStruct2, locations[2]: locationStruct3},
			},
			func(r *registry) string {
				bytes, err := json.Marshal(r.gateways[locations[0]])
				assert.NoError(t, err)
				return string(bytes)
			},
			"key1",
			[]authTokenDbEntry{{"key1", "bucket1,bucket2"}},
		},
		{"auth token with multiple buckets two private gateways",
			&registry{
				gateways:  gatewayMap{locations[0]: []*m.Gateway{gateway1}, locations[1]: []*m.Gateway{gateway3}},
				locations: locationMap{locations[0]: locationStruct1, locations[1]: locationStruct2, locations[2]: locationStruct3},
			},
			func(r *registry) string {
				bytes, err := json.Marshal(r.gateways[locations[0]])
				assert.NoError(t, err)
				return string(bytes)
			},
			"key1",
			[]authTokenDbEntry{{"key1", "bucket1,bucket2"}},
		},
		{"auth token with lastSeenCutoffMillis enabled",
			&registry{
				gateways:  gatewayMap{locations[0]: []*m.Gateway{gateway1}, locations[1]: []*m.Gateway{gateway3}},
				locations: locationMap{locations[0]: locationStruct1, locations[1]: locationStruct2, locations[2]: locationStruct3},
				// Cutoff of 5 seconds
				lastSeenCutoffMillis: 5000,
			},
			func(r *registry) string {
				bytes, err := json.Marshal([]*m.Gateway{gateway1})
				assert.NoError(t, err)
				return string(bytes)
			},
			"key1",
			[]authTokenDbEntry{{"key1", "bucket1,bucket2"}},
		},
	}

	for _, tc := range testTable {
		t.Run(tc.name, func(t *testing.T) {

			dir, err := os.MkdirTemp("", "")
			assert.NoError(t, err)
			defer os.RemoveAll(dir)

			db, err := storage.OpenDatabase(dir + "/db1.sql")
			assert.NoError(t, err)
			defer db.Close()

			stmt, err := db.Prepare("INSERT INTO tokens(key, buckets) VALUES(?, ?)")
			assert.NoError(t, err)

			for _, token := range tc.dbAuthTokens {
				h := sha256.New()
				h.Write([]byte(token.key))
				authTokenHashBytes := h.Sum(nil)

				authTokenHashString := base64.StdEncoding.EncodeToString(authTokenHashBytes)
				_, err = stmt.Exec(authTokenHashString, token.buckets)
				assert.NoError(t, err)
			}

			expectedResponse := tc.expected(tc.mockRegistry)

			e := echo.New()
			e.Use(storageMiddleware(db))
			e.Use(authTokenMiddleware)
			e.GET("/api/5/gateways", tc.mockRegistry.ListAllGateways)
			e.GET("/api/5/gateway", tc.mockRegistry.GatewayPicker)

			// First test ListAllGateways
			req := httptest.NewRequest(http.MethodGet, fmt.Sprintf("/api/5/gateways?loc=%v", url.QueryEscape(locations[0])), nil)
			req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
			if tc.authToken != "" {
				req.Header.Set("x-menshen-auth-token", tc.authToken)
			}
			rec := httptest.NewRecorder()

			e.ServeHTTP(rec, req)
			assert.Equal(t, http.StatusOK, rec.Code)
			assert.Equal(t, strings.TrimSpace(expectedResponse), strings.TrimSpace(rec.Body.String()))

			// The output from GatewayPicker should be the same since we're just filtering by buckets
			req = httptest.NewRequest(http.MethodGet, fmt.Sprintf("/api/5/gateway?loc=%v", url.PathEscape(locations[0])), nil)
			req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
			if tc.authToken != "" {
				req.Header.Set("x-menshen-auth-token", tc.authToken)
			}
			rec = httptest.NewRecorder()

			e.ServeHTTP(rec, req)
			assert.Equal(t, http.StatusOK, rec.Code)
			assert.Equal(t, strings.TrimSpace(expectedResponse), strings.TrimSpace(rec.Body.String()))

			var initialResponse string
			var initialResponseCode int

			// tests for locations
			for _, loc := range locations {
				for i := 0; i < 10; i++ {
					req = httptest.NewRequest(http.MethodGet, fmt.Sprintf("/api/5/gateway?loc=%v", url.PathEscape(loc)), nil)
					req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
					if tc.authToken != "" {
						req.Header.Set("x-menshen-auth-token", tc.authToken)
					}
					rec = httptest.NewRecorder()

					e.ServeHTTP(rec, req)

					if i == 0 {
						initialResponseCode = rec.Code
						initialResponse = strings.TrimSpace(rec.Body.String())
					}
					assert.Equal(t, initialResponseCode, rec.Code)
					assert.Equal(t, initialResponse, strings.TrimSpace(rec.Body.String()))
				}
			}

			// tests for countrycodes
			for _, cc := range countryCodes {
				for i := 0; i < 10; i++ {
					req = httptest.NewRequest(http.MethodGet, fmt.Sprintf("/api/5/gateway?cc=%v", url.PathEscape(cc)), nil)
					req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
					if tc.authToken != "" {
						req.Header.Set("x-menshen-auth-token", tc.authToken)
					}
					rec = httptest.NewRecorder()

					e.ServeHTTP(rec, req)

					if i == 0 {
						initialResponseCode = rec.Code
						initialResponse = strings.TrimSpace(rec.Body.String())
					}
					assert.Equal(t, initialResponseCode, rec.Code)
					assert.Equal(t, initialResponse, strings.TrimSpace(rec.Body.String()))
				}
			}

			// test for random gateways
			for i := 0; i < 10; i++ {
				req = httptest.NewRequest(http.MethodGet, "/api/5/gateway", nil)
				req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
				if tc.authToken != "" {
					req.Header.Set("x-menshen-auth-token", tc.authToken)
				}
				rec = httptest.NewRecorder()

				e.ServeHTTP(rec, req)

				assert.Equal(t, http.StatusOK, rec.Code)
				assert.NotEmpty(t, strings.TrimSpace(rec.Body.String()))
			}
		})
	}
}
