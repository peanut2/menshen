package api

type Config struct {
	EnableCertv3        bool
	AllowGatewayListing bool
	AllowBridgeListing  bool
	AutoTLS             bool
	// EIP is an existing EIPv3 file in the local filesystem (legacy)
	EIP string
	// EIPULR is a remote endpoint where to fetch an existing EIPv3 file from (legacy)
	EIPURL string
	// LocalBridges is a list of addresses where the available bridge
	// containers expose their control ports.
	LocalBridges        []string
	LoadBalancerAddr    string
	Port                int
	PortMetrics         int
	ServerName          string
	ClientCertURL       string
	CaFile              string
	OvpnCaCrt           string
	OvpnCaKey           string
	OvpnClientCrtExpiry int
	// ProviderJson is a file path to an existing provider.json
	ProviderJson   string
	Algo           string
	DBFile         string
	AgentSharedKey string

	// This is the amount of milliseconds to wait since the last heartbeat from a bridge or gateway before
	// removing them from the resources that are returned to clients.
	// If this is zero then no cutoff will be applied
	LastSeenCutoffMillis int64
}

func (c *Config) HasLegacyEIPFile() bool {
	return c.EIP != "" || c.EIPURL != ""
}
