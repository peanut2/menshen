package api

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"

	"0xacab.org/leap/menshen/pkg/genconfig"
	"github.com/labstack/echo/v4"
)

// GenerateConfig godoc.
// @Summary      Fetch OpenVPN Config File.
// @Description  fetch a working config file for OpenVPN service.
// @Tags         Provisioning
// @Produce      text/plain
// @Success      200  {object}  string
// @Failure      400  {object}  error
// @Failure      404  {object}  error
// @Failure      500  {object}  error
// @Router       /api/5/openvpn/config [get]
func GenConfigHelper(registry *registry, cfg *Config) func(echo.Context) error {
	return func(c echo.Context) error {
		return registry.GenerateConfig(c, cfg)
	}
}

func (r *registry) GenerateConfig(c echo.Context, cfg *Config) error {
	conf, err := r.getOpenVPNConfig(cfg)
	if err != nil {
		return err
	}

	// Set Cache-Control header to instruct clients not to cache response
	c.Response().Header().Set("Cache-Control", "no-store")

	return c.String(http.StatusOK, conf)
}

// DownloadConfig godoc.
// @Summary      Fetch OpenVPN Config File.
// @Description  fetch a working config file for OpenVPN service.
// @Tags         Provisioning
// @Produce      text/plain
// @Success      200  {object}  string
// @Failure      400  {object}  error
// @Failure      404  {object}  error
// @Failure      500  {object}  error
// @Router       /api/autoconf [get]
func DownloadConfigHelper(registry *registry, cfg *Config) func(echo.Context) error {
	return func(c echo.Context) error {
		return registry.DownloadConfig(c, cfg)
	}
}

func (r *registry) DownloadConfig(c echo.Context, cfg *Config) error {
	conf, err := r.getOpenVPNConfig(cfg)
	if err != nil {
		return err
	}
	f, err := os.CreateTemp("", "tmp-menshen-")
	if err != nil {
		return err
	}

	if _, err := f.Write([]byte(conf)); err != nil {
		return err
	}

	defer f.Close()
	defer os.Remove(f.Name())

	return c.Attachment(
		f.Name(),
		"leap.ovpn",
	)
}

func (r *registry) getOpenVPNConfig(cfg *Config) (string, error) {
	// TODO : replace with generating config for requested location
	// find the first element in gateways
	var firstElement string
	for key := range r.gateways {
		firstElement = key
		break // exit loop after getting the first element
	}
	gw := r.gateways[firstElement][0]

	var rawCert, CaCrt string
	var err error
	if r.clientCertURL == "" {
		rawCert, err = r.CertWriter(cfg.OvpnCaCrt, cfg.OvpnCaKey, cfg.Algo, cfg.OvpnClientCrtExpiry, true)
		if err != nil {
			log.Printf("getOpenvpnConfig: cert generation error: %s\n", err)
			return rawCert, err
		}

		ca, err := os.ReadFile(cfg.OvpnCaCrt)
		if err != nil {
			log.Fatalf("getOpenvpnConfig: Could not read CA file '%s': %s", cfg.OvpnCaCrt, err)
		}
		CaCrt = string(ca)
	} else {
		req, err := http.NewRequest(http.MethodGet, r.clientCertURL, nil)
		if err != nil {
			log.Printf("genconfig get cert error: %s\n", err)
			return "", err
		}

		res, err := r.client.Do(req)
		if err != nil {
			err := fmt.Errorf("%w, request failed for clientCertURL %s",
				err, r.clientCertURL)
			return "", err
		}
		if res.StatusCode != 200 {
			err := fmt.Errorf("request failed for clientCertURL: %s, got status code: %s,",
				r.clientCertURL, res.Status)
			return "", err
		}

		resBody, err := io.ReadAll(res.Body)
		if err != nil {
			log.Printf("client: could not read response body: %s\n", err)
			return "", err

		}

		rawCert = string(resBody)

		ca, err := os.ReadFile(cfg.CaFile)
		if err != nil {
			log.Fatalf("getOpenvpnConfig: Could not read CA file '%s': %s", cfg.CaFile, err)
		}
		CaCrt = string(ca)
	}

	conf, err := genconfig.SerializeConfig(rawCert, CaCrt, gw)
	if err != nil {
		log.Printf("genconfig serialize error: %s\n", err)
		return "", err
	}
	return conf, nil
}
