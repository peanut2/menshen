package api

import (
	"log"
	"math"
	"math/rand"
	"net/http"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/labstack/echo/v4"
	"golang.org/x/exp/slices"

	"0xacab.org/leap/menshen/pkg/geolocate"
	m "0xacab.org/leap/menshen/pkg/models"
)

var (
	paramCountryCode = "cc"
)

// GatewayPicker godoc
// @Summary      Get gateways by location, countrycode or random
// @Description  Get Gateways with param countrycode for nearest, or with param location to get a gateway in specific location, or a random one without params
// @Tags         Provisioning
// @Accept       json
// @Produce      json
// @Success      200  {object}  []models.Gateway
// @Failure      400  {object}  error
// @Failure      404  {object}  error
// @Failure      500  {object}  error
// @Router       /api/5/gateway [get]
// @Param        cc  query      string    optional  "Country code (ISO-2)"
// @Param        loc     query      string    optional  "location"
// @Security BucketTokenAuth
func (r *registry) GatewayPicker(c echo.Context) error {
	// TODO implement limits
	// TODO implement rate-limiting
	// TODO disable this listing optionally

	countryCode := c.QueryParam("cc")
	location := c.QueryParam("loc")

	var selectedLocation string
	if location != "" {
		// check if requested location is supported
		keys := make([]string, 0, len(r.locations))
		for k := range r.locations {
			keys = append(keys, k)
		}
		if !slices.Contains(keys, location) {
			log.Println("[Debug]: specified location not in r.locations")
			return c.JSON(http.StatusBadRequest, "Location not supported")
		}
		selectedLocation = location
		log.Println("[Debug]: returning gateway for requested location", selectedLocation)
	} else if countryCode != "" {
		// find nearest location for the given countryCode
		log.Println("[Debug]: finding best gateway for Countrycode =", countryCode)
		clientCentroid, err := geolocate.GetCentroidForCountry(countryCode)
		if err != nil {
			return c.JSON(http.StatusBadRequest, "CountryCode not supported")
		}
		minDistance := math.MaxFloat64
		// Iterate through gateway locations and calculate distance to each
		for _, loc := range r.locations {
			gatewayLat, err1 := strconv.ParseFloat(loc.Lat, 64)
			gatewayLon, err2 := strconv.ParseFloat(loc.Lon, 64)
			if err1 != nil || err2 != nil {
				log.Printf("invalid latitude or longitude for location: %s", loc.DisplayName)
			}
			//log.Println(">>", loc.CountryCode, gatewayLat, gatewayLon)
			distance := euclideanDistance(clientCentroid.Lat, clientCentroid.Lon, gatewayLat, gatewayLon)
			log.Println("[Debug]: distance to", loc.Label, "::", distance)

			if distance < minDistance {
				minDistance = distance
				selectedLocation = loc.Label
			}
		}
	} else {
		// choose random location
		log.Println("[Debug]: request without countrycode")
		keys := make([]string, 0, len(r.locations))
		for k := range r.locations {
			keys = append(keys, k)
		}

		log.Println("[Debug]: returning gateway for random location")
		selectedLocation = keys[rand.Intn(len(keys))]
		log.Println("[Debug]: returning gateway for randomly chosen location", selectedLocation)
	}

	gateways := r.gateways[selectedLocation]

	filters := make([]func(*m.Gateway) bool, 0)
	filters = maybeAddGatewayBucketFilter(c, filters)
	filters = maybeAddLastSeenGatewayCutoffFilter(r, filters)
	filtered := filter[*m.Gateway](alltrue(filters), gateways)

	randIndex := 0
	if len(filtered) == 0 {
		return c.JSON(http.StatusOK, filtered)
	} else if len(filtered) > 1 {
		randIndex = rand.Intn(len(filtered))
	}

	var selectedGateway []*m.Gateway
	for _, gPointer := range filtered {
		g := *gPointer
		if g.Host == filtered[randIndex].Host {
			selectedGateway = append(selectedGateway, gPointer)
		}
	}
	return c.JSON(http.StatusOK, selectedGateway)
}

// Function to calculate the Euclidean distance between two points (lat, lon)
func euclideanDistance(lat1, lon1, lat2, lon2 float64) float64 {
	return math.Sqrt(math.Pow(lat2-lat1, 2) + math.Pow(lon2-lon1, 2))
}

var (
	maxGateways          = 4
	paramsGetAllGateways = []string{"tr", "port", "loc"}
)

// ListAllGateways godoc
// @Summary      Get All Gateways
// @Description  Fetch all gateways. This is an optional API endpoint for compatibility with vpnweb, but do not count on all the providers to have it enabled since it makes it easier to enumerate resources. On the other hand, if the service has "open" VPN endpoints, they can enumerate them here freely. Bridges, however, should be more restricted as a general rule.
// @Tags         Provisioning
// @Accept       json
// @Produce      json
// @Param        cc   query  string false "base country code (ISO-2)"
// @Param        tr	  query  string false "transport (tcp|udp)"
// @Param        port query  string false "port"
// @Param        loc  query  string false "location override"
// @Param        byload  query  bool false "Sort by load (experimental)"
// @Success      200  {object}  []models.Gateway
// @Failure      400  {object}  error
// @Failure      404  {object}  error
// @Failure      500  {object}  error
// @Router       /api/5/gateways [get]
// @Security BucketTokenAuth
func (r *registry) ListAllGateways(c echo.Context) error {
	cc := sanitizeCountryCode(c.QueryParam(paramCountryCode))
	doIncrementCountryHit(cc)

	var hasLocationOverride bool
	if c.QueryParam("loc") != "" {
		hasLocationOverride = true
	}

	// Step 1. We start with all the gateways
	gateways := r.AllGateways()

	var selectedLocationLabels []string
	if hasLocationOverride {
		selectedLocationLabels = []string{c.QueryParam("loc")}
	} else {
		selectedLocationLabels = getLabelsForLocations(
			geolocate.PickBestLocations(r.lm, cc, r.AllLocations()))
	}
	log.Printf("Selecting gateways from locations: %v", selectedLocationLabels)

	// Step 2a. We pass the pre-filter location selection to load balancer.
	// If load filtering is not enabled, we just apply a filter for the selected locations.
	// This should be the default in the near future, but this toggle switch allows us to
	// test for the time being.
	byload := c.QueryParam("byload")
	if byload == "1" {
		// We pass only a subset of the gateways to the geolocation picker
		gatewaysByLoad, err := r.lb.SortGateways(selectedLocationLabels, gateways)
		if err != nil {
			log.Printf("Load lookup error: %v", err)
			// we didn't succeed, so need to fallback to location-bucket selection.
			// this usually means that the load balancer is not receiving updates
			// from the agents matching the labels we've passed.
			byload = "0"
		} else {
			gateways = gatewaysByLoad
		}
	}

	// Step 2b. Load selection mechanism not selected, or failed above. we apply an OR filter
	// for all the pre-selected locations.
	if byload == "0" {
		filteredByLoc := filter[*m.Gateway](
			func(gw *m.Gateway) bool {
				return slices.Contains(selectedLocationLabels, gw.Location)
			},
			gateways)
		if len(filteredByLoc) != 0 {
			gateways = filteredByLoc
		}
	}

	// Step 3. We apply other possible filters (transport, port or location override)
	// This step might be better applied before the previous one.
	filters := gatewayFiltersFromParams(c, paramsGetAllGateways)
	filters = maybeAddGatewayBucketFilter(c, filters)
	filters = maybeAddLastSeenGatewayCutoffFilter(r, filters)
	result := filter[*m.Gateway](alltrue(filters), gateways)

	// Step 4. If we have a location override, sort by load.
	if hasLocationOverride {
		sort.Slice(result, func(i, j int) bool {
			return result[i].Load < result[j].Load
		})
	}

	// Step 5. Do we have <= maxGateways? If so, return
	if len(result) <= maxGateways {
		return c.JSON(http.StatusOK, result)
	}

	// Step 6. No manual override, and > maxGateways.
	// TOOD we might do several passes, or we might just sort by load, get the lower slice
	// and randomize.
	filterHighLoad := []func(*m.Gateway) bool{
		func(gw *m.Gateway) bool {
			return !gw.Overloaded
		},
		func(gw *m.Gateway) bool {
			return gw.Load < 0.8
		},
	}
	lowerLoad := filter[*m.Gateway](alltrue(filterHighLoad), result)
	if len(lowerLoad) != 0 {
		result = lowerLoad
	}
	if len(result) <= maxGateways {
		return c.JSON(http.StatusOK, result)
	}

	// randomize the remaining slice to ensure we get more than one location
	sort.Slice(result, func(i, j int) bool {
		return rand.Intn(100) > 50
	})
	return c.JSON(http.StatusOK, result[:maxGateways])
}

func getLabelsForLocations(locations []*m.Location) []string {
	labels := []string{}
	for _, loc := range locations {
		labels = append(labels, loc.Label)
	}
	return labels
}

func gatewayFiltersFromParams(c echo.Context, params []string) []func(*m.Gateway) bool {
	filters := make([]func(*m.Gateway) bool, 0)
	for _, param := range params {
		filters = maybeAddGatewayFilter(c, param, filters)
	}
	return filters
}

// maybeAddGatewayFilter receives a context object, a parameter string, and an array of filters. If the query parameter is found, it expands the filter array by adding the matching gateway filter.
func maybeAddGatewayFilter(c echo.Context, param string, filters []func(*m.Gateway) bool) []func(*m.Gateway) bool {
	// get the query parameter
	q := c.QueryParam(param)
	if q == "" {
		// empty, return the array of filters
		return filters
	}

	var filter m.EndpointFilter[m.Gateway]
	switch param {
	case "tr":
		filter = func(gw *m.Gateway) bool {
			return gw.Transport == q
		}
	case "port":
		// TODO: we could also consider a special comma-separated "ports". keeeping it simple for now
		port, err := strconv.Atoi(q)
		if err != nil {
			return filters
		}
		filter = func(gw *m.Gateway) bool {
			return gw.Port == port
		}
	case "loc":
		filter = func(gw *m.Gateway) bool {
			return gw.Location == q
		}

	default:
		// not a known filter
		return filters
	}
	return append(filters, filter)
}

func maybeAddLastSeenGatewayCutoffFilter(r *registry, filters []func(*m.Gateway) bool) []func(*m.Gateway) bool {
	if r.lastSeenCutoffMillis == 0 {
		return filters
	}

	return append(filters, func(b *m.Gateway) bool {
		nowMillis := time.Now().UnixMilli()

		return b.LastSeenMillis >= nowMillis-r.lastSeenCutoffMillis
	})
}

func maybeAddGatewayBucketFilter(c echo.Context, filters []func(*m.Gateway) bool) []func(*m.Gateway) bool {
	buckets, ok := c.Get("buckets").([]string)
	if !ok {
		buckets = []string{""}
	}

	return append(filters, func(b *m.Gateway) bool {
		if b.Bucket == "" {
			return true
		}
		for _, bucket := range buckets {
			if b.Bucket == bucket {
				return true
			}
		}
		return false
	})
}

// sanitizeCountryCode will check if the passed ISO-2 country code is known to us.
// For empty strings, they stay the same.
// For non-valid country codes, it will return the string "--". Otherwise, will return the original string.
func sanitizeCountryCode(cc string) string {
	if cc == "" {
		return ""
	}
	if len(cc) != 2 {
		return "--"
	}
	cc = strings.ToUpper(cc)
	if !geolocate.IsValidCountryCode(cc) {
		return "--"
	}
	return cc
}
