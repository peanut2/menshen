package api

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

// @Summary      Get provider info
// @Description  Fetch provider information how to configure and bootstrap the VPN
// @Tags         Provisioning
// @Accept       json
// @Produce      json
// @Success      200  {object}  models.Provider
// @Failure      400  {object}  error
// @Failure      404  {object}  error
// @Failure      500  {object}  error
// @Router       /provider.json [get]
func (r *registry) GetProvider(c echo.Context) error {
	return c.JSON(http.StatusOK, r.provider)
}
