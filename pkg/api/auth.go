package api

import (
	"bytes"
	"crypto/hmac"
	"crypto/sha256"
	"crypto/subtle"
	"encoding/base64"
	"encoding/hex"
	"io"
	"net/http"
	"strings"

	"github.com/apex/log"
	"github.com/labstack/echo/v4"
)

func authTokenMiddleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		db, err := getDBFromContext(c)
		if err != nil {
			log.Errorf("Error getting database from context")
			return next(c)
		}

		authToken := c.Request().Header.Get("x-menshen-auth-token")

		if authToken != "" {
			h := sha256.New()
			h.Write([]byte(authToken))
			authTokenHashBytes := h.Sum(nil)

			authTokenHashString := base64.StdEncoding.EncodeToString(authTokenHashBytes)

			var buckets string
			if err := db.QueryRow("SELECT buckets FROM tokens WHERE key = ?", authTokenHashString).Scan(&buckets); err != nil {
				log.Errorf("Error querying for access token: %v", err)
			} else {
				bucketsSlice := strings.Split(buckets, ",")
				c.Set("buckets", bucketsSlice)
			}
		}

		return next(c)
	}
}

func agentRegistrationMiddleware(sharedSecret string) func(echo.HandlerFunc) echo.HandlerFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			hmacHeader := c.Request().Header.Get("x-menshen-agent-auth")

			hmacHeaderBytes := make([]byte, hex.DecodedLen(len(hmacHeader)))
			_, err := hex.Decode(hmacHeaderBytes, []byte(hmacHeader))
			if err != nil {
				log.Errorf("Error decoding hmac auth bytes: %v", err)
				return echo.NewHTTPError(http.StatusUnauthorized, "Please provide valid credentials")
			}

			reqBody := []byte{}
			if c.Request().Body != nil {
				reqBody, _ = io.ReadAll(c.Request().Body)
			}
			c.Request().Body = io.NopCloser(bytes.NewBuffer(reqBody)) // We need to reset the request body reader
			computed := hmac.New(sha256.New, []byte(sharedSecret))
			computed.Write(reqBody)
			calculatedHMAC := []byte(computed.Sum(nil))

			if subtle.ConstantTimeCompare(hmacHeaderBytes, calculatedHMAC) == 0 {
				log.Errorf("HMAC provided invalid. Provided: %v, expected: %v", hmacHeader, hex.EncodeToString(calculatedHMAC))
				return echo.NewHTTPError(http.StatusUnauthorized, "Please provide valid credentials")
			}

			return next(c)
		}
	}
}
