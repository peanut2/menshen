// Copyright (c) 2023 LEAP Encryption Access Project
// Copyright (c) 2023 kali kaneko
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package loadbalancer

import (
	"context"
	"math"
	"net"
	"time"

	"0xacab.org/leap/menshen/pkg/models"
	"git.autistici.org/ale/lb"
	lbpb "git.autistici.org/ale/lb/proto"
	"github.com/apex/log"
	"google.golang.org/grpc"
)

// FIXME: limits, fullness and timeout for now are hardcoded
//
//	we'll explore if they need to be configurable
const (
	fullness = 0.9
	timeout  = 10 * time.Minute
)

var limits = []*lb.Limit{
	{
		Dimension: "bandwidth_tx",
		Min:       0,
		Max:       1,
	},
	{
		Dimension: "bandwidth_rx",
		Min:       0,
		Max:       1,
	},
	{
		Dimension: "packets_tx",
		Min:       0,
		Max:       1,
	},
	{
		Dimension: "packets_rx",
		Min:       0,
		Max:       1,
	},
	{
		Dimension: "conntrack",
		Min:       0,
		Max:       1,
	},
	{
		Dimension: "clients",
		Min:       0,
		Max:       200,
	},
}

// LoadBalancer contains a reference to an instance of ale's lb generic load
// balancing algorithm, and to the grpc server that will receive updates from
// the agents.
type LoadBalancer struct {
	server   *grpc.Server
	balancer *lb.Loadbalancer
}

// StartLoadBalancer is a constructor for LoadBalancer. It expects a bind
// address, and it returns a pointer to a LoadBalancer that is already started,
// and any error that was raised during the operation.
func StartLoadBalancer(bindAddr string) (*LoadBalancer, error) {
	server := grpc.NewServer()
	balancer := lb.New(limits, fullness, timeout)
	lbpb.RegisterLoadBalancerServer(server, balancer)

	listener, err := net.Listen("tcp", bindAddr)
	if err != nil {
		return nil, err
	}
	go func() {
		err := server.Serve(listener)
		if err != nil {
			log.WithError(err).Error("grpc server Serve() failed")
		}
	}()

	return &LoadBalancer{
		server:   server,
		balancer: balancer,
	}, nil
}

// Stop does stop the underlying grpc server.
func (l *LoadBalancer) Stop() {
	l.server.Stop()
}

// SortGateways is the main function that allows to use the load balancer for
// now. It receives an array of locations (already sorted by whatever criteria,
// like proximity), and it returns an array of `GatewayLoad` that already
// contains up-to-date load and overloaded information.
func (l *LoadBalancer) SortGateways(
	sortedLocations []string,
	gateways []*models.Gateway) ([]*models.Gateway, error) {

	// to begin with, we filter by only the location tag
	tags := make([]*lbpb.Tag, len(sortedLocations))

	for i, location := range sortedLocations {
		tags[i] = &lbpb.Tag{
			Key:   "location",
			Value: location,
		}
	}

	resp, err := l.balancer.ListSelection(context.TODO(), &lbpb.ListRequest{
		Select: &lbpb.SelectRequest{
			Tags: tags,
		},
	})
	if err != nil {
		return []*models.Gateway{}, err
	}

	gatewaysWithLoad := []*models.Gateway{}

	// we iterate over all the response nodes, and update the load
	// value in the passed []*Gateway array. This function has a side
	// effect, and it's that we're updating the fullness/overload flags
	// directly in the objects that we're passed. We should better make a disposable
	// copy and return that.

	for _, node := range resp.Nodes {
		for _, gw := range gateways {
			if gw.Host == node.NodeName {
				gw.Load = sanitize(node.Fullness)
				gw.Overloaded = node.Overload
				gatewaysWithLoad = append(gatewaysWithLoad, gw)
			}
		}
	}
	return gatewaysWithLoad, nil
}

func sanitize(val float64) float64 {
	if val == math.Inf(1) {
		return 1
	}
	return toFixed(val, 3)
}

func toFixed(num float64, precision int) float64 {
	output := math.Pow(10, float64(precision))
	return float64(math.Round(num*output)) / output
}
