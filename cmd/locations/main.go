// geolocation utility to add coordinates to an eip-service inventory.
package main

import (
	"fmt"
	"log"
	"os"

	"0xacab.org/leap/menshen/pkg/api"
	"0xacab.org/leap/menshen/pkg/geolocate"
	"0xacab.org/leap/menshen/pkg/models"
)

func main() {
	if len(os.Args) != 3 {
		log.Fatal("usage: locations eip.json <cmd> <cc>")
	}

	eip := os.Args[1]
	cmd := os.Args[2]

	eipData, err := api.ParseEIPServiceFile(eip)
	if err != nil {
		panic(err)
	}

	switch cmd {
	case "get-location":
		geolocateAllLocations(eipData)
	}
}

func geolocateAllLocations(eipData *models.EIPServiceV3) {
	locations := api.GetLocationsFromEIPData(eipData)
	targets := make([]string, 0)

	for loc := range locations {
		targets = append(targets, loc)
	}

	coords := geolocate.GeolocateCities(targets)

	for name, loc := range locations {
		coord := coords[name]
		loc.Lat = fmt.Sprintf("%.2f", coord.Lat)
		loc.Lon = fmt.Sprintf("%.2f", coord.Lon)
	}

	for _, loc := range locations {
		var sep string
		if len(loc.DisplayName) > 7 {
			sep = "\t"

		} else {
			sep = "\t\t"
		}
		fmt.Printf("%s"+sep+"%v\t%v\n", loc.DisplayName, loc.Lat, loc.Lon)
	}
}
