package cmd

import (
	"bufio"
	"fmt"
	"os"
	"strings"

	"0xacab.org/leap/menshen/storage"
	"github.com/apex/log"
	"github.com/spf13/cobra"
)

var deleteCmd = &cobra.Command{
	Use:   "delete",
	Short: "Delete Invite Tokens in db by bucket(s) or by key",
	Run:   doDeleteInviteToken,
}

var deleteBucket string
var deleteToken string

func init() {
	rootCmd.AddCommand(deleteCmd)
	deleteCmd.Flags().StringVarP(&deleteBucket, "bucket", "b", "", "Delete all invite tokens in the db that belong to the given bucket")
	deleteCmd.Flags().StringVarP(&deleteToken, "token", "t", "", "Delete invite token(s) with the specified invite token. Use - to enter invite code manually")
}

func doDeleteInviteToken(cmd *cobra.Command, args []string) {
	db, err := storage.OpenDatabase(dbPath)
	failOnError("Could not open database", err)
	defer db.Close()

	if len(deleteBucket) != 0 {
		deletedRows, err := deleteInviteTokenByBucket(db, deleteBucket)
		failOnError("Could not delete Invite Tokens", err)
		log.Infof("Successfully deleted %d invite token(s) with bucket %q", deletedRows, deleteBucket)
	} else if len(deleteToken) != 0 {
		if deleteToken == "-" {
			reader := bufio.NewReader(os.Stdin)
			fmt.Print("Enter invite token: ")
			deleteToken, err = reader.ReadString('\n')
			failOnError("Could not read string from command line", err)
			deleteToken = strings.TrimSpace(deleteToken)
		}
		inviteTokenHashed := hashInviteToken(deleteToken)
		deletedRows, err := deleteInviteTokenByKey(db, inviteTokenHashed)
		failOnError("Could not delete invite token", err)
		log.Infof("Successfully deleted %d invite token(s) with token %q", deletedRows, deleteToken)

	} else {
		log.Error("You must specify at least parameter")
		cmd.Help() // nolint: errcheck
	}
}
