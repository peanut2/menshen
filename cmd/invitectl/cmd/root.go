package cmd

import (
	"os"

	"github.com/apex/log"
	"github.com/apex/log/handlers/cli"
	"github.com/spf13/cobra"
)

var dbPath string

var rootCmd = &cobra.Command{
	Use:   "invitectl",
	Short: "A simple cli tool to manage invite tokens for LEAP VPN",
}

func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	log.SetHandler(cli.New(os.Stdout))
	rootCmd.PersistentFlags().StringVar(&dbPath, "db", "", "Path to sqlite database. Creates a new db if file does not exist (required)")
	if envDB := os.Getenv("DB"); envDB != "" {
		dbPath = envDB
	}

}
