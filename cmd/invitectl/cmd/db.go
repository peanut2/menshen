package cmd

import (
	"fmt"
	"strings"

	"github.com/jmoiron/sqlx"
)

func getInviteTokens(db *sqlx.DB) ([]InviteToken, error) {
	var tokens []InviteToken

	dbInviteTokens, err := db.Query("SELECT * FROM tokens")
	if err != nil {
		return []InviteToken{}, fmt.Errorf("Could not create SELECT query: %s", err)
	}
	defer dbInviteTokens.Close()

	for dbInviteTokens.Next() {
		var token InviteToken
		var bucketsCommaSeperated string

		err = dbInviteTokens.Scan(&token.Key, &bucketsCommaSeperated)
		if err != nil {
			return []InviteToken{}, fmt.Errorf("Could not read from database: %s", err)
		}
		token.Buckets = strings.Split(bucketsCommaSeperated, ",")
		tokens = append(tokens, token)
	}
	return tokens, nil
}

func insertInviteToken(db *sqlx.DB, token InviteToken) error {
	err := token.Validate()
	if err != nil {
		return fmt.Errorf("Could insert token into database. Invalid token: %s", err)
	}

	tx, err := db.Begin()
	if err != nil {
		return fmt.Errorf("Could not begin database transaction: %s", err)
	}

	stmt, err := tx.Prepare(`INSERT INTO tokens(key, buckets) VALUES(?, ?)`)
	if err != nil {
		return fmt.Errorf("Could not create prepared statement: %s", err)
	}
	defer stmt.Close()

	_, err = stmt.Exec(token.Key, strings.Join(token.Buckets, ","))
	if err != nil {
		return fmt.Errorf("Could not execute prepared statement: %s", err)
	}

	err = tx.Commit()
	if err != nil {
		return fmt.Errorf("Could not commit db statement: %s", err)
	}
	return nil
}

func deleteAllInviteTokens(db *sqlx.DB) error {
	_, err := db.Exec(`DELETE FROM tokens`)
	if err != nil {
		return err
	}
	return nil
}

func deleteInviteTokenByBucket(db *sqlx.DB, bucket string) (int64, error) {
	tx, err := db.Begin()
	if err != nil {
		return 0, fmt.Errorf("Could not begin database transaction: %s", err)
	}

	stmt, err := tx.Prepare(`DELETE from tokens WHERE buckets like ?`)
	if err != nil {
		return 0, fmt.Errorf("Could not create prepared statement: %s", err)
	}
	defer stmt.Close()

	result, err := stmt.Exec(fmt.Sprintf("%%%s%%", bucket))
	if err != nil {
		return 0, fmt.Errorf("Could not execute prepared statement: %s", err)
	}

	deletedRows, err := result.RowsAffected()
	if err != nil {
		return 0, fmt.Errorf("Could not get amount of affected rows: %s", err)
	}

	err = tx.Commit()
	if err != nil {
		return 0, fmt.Errorf("Could not commit db statement: %s", err)
	}
	return deletedRows, nil
}

func deleteInviteTokenByKey(db *sqlx.DB, key string) (int64, error) {
	tx, err := db.Begin()
	if err != nil {
		return 0, fmt.Errorf("Could not begin database transaction: %s", err)
	}

	stmt, err := tx.Prepare(`DELETE from tokens WHERE key = ?`)
	if err != nil {
		return 0, fmt.Errorf("Could not create prepared statement: %s", err)
	}
	defer stmt.Close()

	result, err := stmt.Exec(key)
	if err != nil {
		return 0, fmt.Errorf("Could not execute prepared statement: %s", err)
	}

	deletedRows, err := result.RowsAffected()
	if err != nil {
		return 0, fmt.Errorf("Could not get amount of affected rows: %s", err)
	}

	err = tx.Commit()
	if err != nil {
		return 0, fmt.Errorf("Could not commit db statement: %s", err)
	}
	return deletedRows, nil
}
