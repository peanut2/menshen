package cmd

import (
	"strings"

	"0xacab.org/leap/menshen/storage"
	"github.com/apex/log"
	"github.com/spf13/cobra"
)

var listCmd = &cobra.Command{
	Use:   "list",
	Short: "List all invite tokens stored in database",
	Run:   doPrintAllInviteTokens,
}

func init() {
	rootCmd.AddCommand(listCmd)
}

func doPrintAllInviteTokens(cmd *cobra.Command, args []string) {
	db, err := storage.OpenDatabase(dbPath)
	failOnError("Could not open database", err)
	defer db.Close()

	tokens, err := getInviteTokens(db)
	failOnError("Could not get invite tokens from db", err)

	log.Infof("%60s %s", "invite token (hashed)", "buckets")
	for _, token := range tokens {
		log.Infof("%60s %s", token.Key, strings.Join(token.Buckets, " "))
	}
}
