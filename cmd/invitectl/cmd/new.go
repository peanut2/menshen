package cmd

import (
	"os"
	"strings"

	"0xacab.org/leap/bitmask-core/pkg/introducer"
	"0xacab.org/leap/menshen/storage"
	"github.com/apex/log"
	"github.com/spf13/cobra"
	"gopkg.in/yaml.v2"
)

var newCmd = &cobra.Command{
	Use:   "new",
	Short: "Add new invite token to database",
	Run:   doAddInviteToken,
}

var logDebug bool
var tokenPrefix string
var buckets string
var numberOfTokens int
var keyLength int
var inviteCodeParameter string

func init() {
	rootCmd.AddCommand(newCmd)
	newCmd.Flags().BoolVarP(&logDebug, "debug", "d", false, "Enable debug logging")
	newCmd.Flags().StringVarP(&inviteCodeParameter, "invite-code-parameter", "c", "", "Yaml file with invite code parameters to print the whole invite code")
	newCmd.Flags().StringVarP(&tokenPrefix, "prefix", "p", "solitech_", "Static prefix used for all generated invite tokens")
	newCmd.Flags().StringVarP(&buckets, "buckets", "b", "", "Comma separated list of buckets")
	newCmd.Flags().IntVarP(&numberOfTokens, "number", "n", 1, "Number of invite tokens to create")
	newCmd.Flags().IntVarP(&keyLength, "key-length", "l", 16, "Number of random bytes for a generated invite token")
	newCmd.MarkFlagRequired("invite-code-parameter") // nolint: errcheck
	newCmd.MarkFlagRequired("buckets")               // nolint: errcheck

}

func doAddInviteToken(cmd *cobra.Command, args []string) {
	if logDebug {
		log.SetLevel(log.DebugLevel)
	}

	db, err := storage.OpenDatabase(dbPath)
	failOnError("Could not open database", err)
	defer db.Close()

	log.Warn("Please keep in mind that invite tokens are saved as hash and can only printed once!")
	intro := getIntroducerParameters()

	for range numberOfTokens {
		inviteTokenPlain, inviteTokenHashed, err := generateInviteToken(tokenPrefix, keyLength)
		failOnError("Could not generate random key", err)
		log.Infof("Generated invite token %s for buckets %q", inviteTokenPlain, buckets)

		intro.Auth = inviteTokenPlain
		err = intro.Validate()
		failOnError("Could not validate invite code parameters from file: %s", err)
		log.Infof("Invite code: %s", intro.URL())

		inviteToken := InviteToken{
			Key:     inviteTokenHashed,
			Buckets: strings.Split(buckets, ","),
		}
		err = insertInviteToken(db, inviteToken)
		failOnError("Could not insert invite token", err)
		log.Debugf("Successfully inserted invite token to database")
	}
}

func getIntroducerParameters() *introducer.Introducer {
	yamlFile, err := os.ReadFile(inviteCodeParameter)
	failOnError("Could not read invite code parameters from file", err)

	var introducer introducer.Introducer
	err = yaml.Unmarshal(yamlFile, &introducer)
	failOnError("Could not yaml decode invite code parameters from file", err)
	return &introducer
}
