package cmd

import (
	"crypto/rand"
	"crypto/sha256"
	"encoding/base64"
	"errors"
	"fmt"
	"os"
	"strings"

	"github.com/apex/log"
)

type InviteToken struct {
	Key     string // the actual Invite Code, called Key in the db
	Buckets []string
}

func (t InviteToken) String() string {
	return fmt.Sprintf("InviteToken(key=%q, buckets=%s)", t.Key, strings.Join(t.Buckets, ","))
}

func (t InviteToken) Validate() error {
	if len(t.Key) == 0 {
		return errors.New("Invite Token is empty")
	}

	if strings.Contains(t.Key, " ") {
		return errors.New("Invite Token contains space")
	}

	if len(t.Buckets) == 0 {
		return errors.New("The Invite Token does not have bucket assigned")
	}

	for _, bucket := range t.Buckets {
		if strings.Contains(bucket, " ") {
			return fmt.Errorf("Invite Bucket should not contain spaces (%q)", bucket)
		}
	}
	return nil
}

func failOnError(msg string, err error) {
	if err != nil {
		log.Fatalf("ERROR: %s: %s", msg, err)
		os.Exit(1)
	}
}

func hashInviteToken(inviteToken string) string {
	h := sha256.New()
	h.Write([]byte(inviteToken))
	inviteTokenHashed := h.Sum(nil)
	inviteTokenEncoded := base64.StdEncoding.EncodeToString(inviteTokenHashed)
	log.Debugf("SHA265 hashed and base64 encoded Invite Token (saved in db): %s", inviteTokenEncoded)
	return inviteTokenEncoded
}

// returns the Invite Token, hashed Invite Token and error
// the plain Invite Token will be printed with the whole Invite Code
// the hashed Invite Token gets saved to disk
func generateInviteToken(prefix string, keyLength int) (string, string, error) {
	b := make([]byte, keyLength)
	_, err := rand.Read(b)
	if err != nil {
		return "", "", err
	}
	inviteToken := fmt.Sprintf("%s%s", prefix, base64.StdEncoding.EncodeToString(b))
	inviteTokenHashed := hashInviteToken(inviteToken)
	return inviteToken, inviteTokenHashed, nil
}
