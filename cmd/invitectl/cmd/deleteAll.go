package cmd

import (
	"0xacab.org/leap/menshen/storage"
	"github.com/apex/log"
	"github.com/spf13/cobra"
)

var deleteAllCmd = &cobra.Command{
	Use:   "delete-all",
	Short: "Delete all invite tokens in database",
	Run:   doDeleteAllInviteTokens,
}

func init() {
	rootCmd.AddCommand(deleteAllCmd)
}

func doDeleteAllInviteTokens(cmd *cobra.Command, args []string) {
	db, err := storage.OpenDatabase(dbPath)
	failOnError("Could not open database", err)
	defer db.Close()

	err = deleteAllInviteTokens(db)
	failOnError("Could not delete all invite tokens", err)
	log.Info("Successfully deleted all invite tokens in database")
}
