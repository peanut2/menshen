package main

import (
	"flag"
	"fmt"
	"os"
	"strings"

	"github.com/apex/log"
	cliHandler "github.com/apex/log/handlers/cli"

	"github.com/spf13/pflag"
	"github.com/spf13/viper"

	_ "0xacab.org/leap/menshen/api"
	api "0xacab.org/leap/menshen/pkg/api"
)

var (
	// when adding new parameters, please make sure
	// to manually add the parameters to the environment bindings for viper in the main function.
	addrLoadBalancer     = "lb-addr"
	enableCertv3         = "enable-cert-v3"
	allowBridgeList      = "allow-bridge-list"
	allowGatewayList     = "allow-gateway-list"
	autoTLS              = "auto-tls"
	fromEIPFile          = "from-eip-file"
	fromEIPURL           = "from-eip-url"
	fromProviderJsonFile = "from-provider-json-file"
	// localBridges is a comma-separated list of addresses for the control port of the bridge nodes.
	localBridges         = "localbridges"
	port                 = "port"
	portMetrics          = "metrics-port"
	serverName           = "server-name"
	verbose              = "verbose"
	clientCertURL        = "client-cert-url"
	caFile               = "ca-file"
	ovpnCaCrt            = "ovpn-ca-crt"
	ovpnCaKey            = "ovpn-ca-key"
	ovpnClientCrtExpiry  = "ovpn-client-crt-expiry"
	algo                 = "algo"
	dbFile               = "db-file"
	agentSharedKey       = "agent-shared-key"
	lastSeenCutoffMillis = "last-seen-cutoff-millis"
)

// @version 0.5.2
// @description This is a LEAP VPN Service API
// @termsOfService https://leap.se/terms/
// @title Menshen API
// @host localhost:1323
// @basePath /
// @contact.name API Support
// @contact.url http://leap.se/support
// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html
// @securityDefinitions.apikey AgentRegistrationAuth
// @in header
// @name x-menshen-agent-auth
// @description This is an HMAC based webhook authentication that uses a shared private key to ensure the authenticity of agent registration messages.
// @securityDefinitions.apikey BucketTokenAuth
// @in header
// @name x-menshen-auth-token
// @description This is an auth token that corresponds to access to particular resources which are delineated by "buckets".
func main() {
	flag.Int(port, 8443, "port to listen on")
	flag.Int(portMetrics, 9002, "port for /metrics (prometheus) to listen on")
	flag.String(addrLoadBalancer, ":9003", "Address for load balancer to listen on")

	flag.Bool(enableCertv3, false, "enable /3/cert endpoint for rsa cert generation")
	flag.Bool(allowGatewayList, false, "allow public gateway listing")
	flag.Bool(allowBridgeList, false, "allow public bridge listing")
	flag.Bool(autoTLS, false, "configure auto TLS using Lets Encrypt")
	flag.String(localBridges, "", "comma-separated list of addresses for the control port of bridges")
	flag.String(fromEIPFile, "", "start from eip-service file (legacy)")
	flag.String(fromEIPURL, "", "start from eip-service url (legacy)")
	flag.String(fromProviderJsonFile, "", "file path to provider.json")
	flag.String(clientCertURL, "", "url that returns a valid OpenVPN certificate and private key in plain text")
	flag.String(caFile, "", "filename with CA certificate used for validating certificates")
	flag.String(ovpnCaCrt, "", "openvpn ca crt for client cert generation")
	flag.String(ovpnCaKey, "", "openvpn ca key for signing client cert")
	flag.Int(ovpnClientCrtExpiry, 23, "openvpn client cert expiry")
	flag.String(algo, "ed25519", "Select the preferred algorithm for certificate generation currently supported ecdsa, ed25519")
	flag.String(serverName, "", "server name (for LetsEncrypt AutoTLS)")
	flag.String(dbFile, "./sqlite.db", "location of sqlite database file")
	flag.String(agentSharedKey, "", "Shared key for verifying HMAC of agent registration requests. If empty, will disable agent registration.")
	flag.Int64(lastSeenCutoffMillis, 0, "the amount of milliseconds to wait since the last heartbeat from a bridge or gateway before removing them from the resources that are returned to clients. If this is zero then no cutoff will be applied")
	flag.Bool(verbose, false, "set log verbosity to DEBUG")

	pflag.CommandLine.AddGoFlagSet(flag.CommandLine)
	pflag.Parse()
	err := viper.BindPFlags(pflag.CommandLine)
	if err != nil {
		log.WithError(err).Fatal("Failed to BindPFlags")
	}

	viper.AddConfigPath(".")
	viper.SetConfigName("menshen")
	viper.SetConfigType("yaml")
	err = viper.ReadInConfig()
	if err != nil {
		log.WithError(err).Warn("Failed to ReadInConfig")
	}

	// allow to specify flags from environment variables too
	// AutoEnv() should work, but somehow seems to break for flags with
	// dashes. So resorting to manual binding. Looking for other alternatives like knadh/koanf
	// might be recommendable.
	// TODO this could also be automated
	viper.SetEnvPrefix("MENSHEN")
	//nolint:ineffassign,staticcheck // It's annoying to write a if err != nil block for every single one of these 🤷
	{
		err = viper.BindEnv(port, "MENSHEN_PORT")
		err = viper.BindEnv(portMetrics, "MENSHEN_METRICS_PORT")
		err = viper.BindEnv(addrLoadBalancer, "MENSHEN_LB_ADDR")
		err = viper.BindEnv(enableCertv3, "ENABLE_CERT_V3")
		err = viper.BindEnv(allowGatewayList, "MENSHEN_ALLOW_GATEWAY_LIST")
		err = viper.BindEnv(allowBridgeList, "MENSHEN_ALLOW_BRIDGE_LIST")
		err = viper.BindEnv(autoTLS, "MENSHEN_AUTO_TLS")
		err = viper.BindEnv(localBridges, "MENSHEN_LOCALBRIDGES")
		err = viper.BindEnv(fromEIPFile, "MENSHEN_FROM_EIP_FILE")
		err = viper.BindEnv(fromEIPURL, "MENSHEN_FROM_EIP_URL")
		err = viper.BindEnv(fromProviderJsonFile, "MENSHEN_FROM_PROVIDER_JSON_FILE")
		err = viper.BindEnv(clientCertURL, "MENSHEN_CLIENT_CERT_URL")
		err = viper.BindEnv(caFile, "MENSHEN_CA_FILE")
		err = viper.BindEnv(ovpnCaCrt, "MENSHEN_OVPN_CA_CRT")
		err = viper.BindEnv(ovpnCaKey, "MENSHEN_OVPN_CA_KEY")
		err = viper.BindEnv(ovpnClientCrtExpiry, "MENSHEN_OVPN_CLIENT_CRT_EXPIRY")
		err = viper.BindEnv(algo, "MENSHEN_ALGO")
		err = viper.BindEnv(serverName, "MENSHEN_SERVER_NAME")
		err = viper.BindEnv(dbFile, "MENSHEN_DB_FILE")
		err = viper.BindEnv(agentSharedKey, "MENSHEN_AGENT_SHARED_KEY")
		err = viper.BindEnv(lastSeenCutoffMillis, "MENSHEN_LAST_SEEN_CUTOFF_MILLIS")
		err = viper.BindEnv(verbose, "MENSHEN_VERBOSE")
	}
	if err != nil {
		log.WithError(err).Fatal("Failed to BindEnv")
	}

	// for the few single-word flags at least
	viper.AutomaticEnv()

	// Start getting configuration valuess

	verbose := viper.GetBool(verbose)
	if verbose {
		log.SetLevel(log.DebugLevel)
	}
	log.SetHandler(cliHandler.New(os.Stdout))

	lbAddr := viper.GetString(addrLoadBalancer)

	bridgesStr := viper.GetString(localBridges)
	bridges := strings.Split(bridgesStr, ",")

	cfg := &api.Config{
		EnableCertv3:         viper.GetBool(enableCertv3),
		AllowGatewayListing:  viper.GetBool(allowGatewayList),
		AllowBridgeListing:   viper.GetBool(allowBridgeList),
		AutoTLS:              viper.GetBool(autoTLS),
		ServerName:           viper.GetString(serverName),
		EIP:                  viper.GetString(fromEIPFile),
		EIPURL:               viper.GetString(fromEIPURL),
		ProviderJson:         viper.GetString(fromProviderJsonFile),
		LocalBridges:         bridges,
		Port:                 viper.GetInt(port),
		PortMetrics:          viper.GetInt(portMetrics),
		LoadBalancerAddr:     lbAddr,
		ClientCertURL:        viper.GetString(clientCertURL),
		CaFile:               viper.GetString(caFile),
		OvpnCaCrt:            viper.GetString(ovpnCaCrt),
		OvpnCaKey:            viper.GetString(ovpnCaKey),
		OvpnClientCrtExpiry:  viper.GetInt(ovpnClientCrtExpiry),
		Algo:                 viper.GetString(algo),
		DBFile:               viper.GetString(dbFile),
		AgentSharedKey:       viper.GetString(agentSharedKey),
		LastSeenCutoffMillis: viper.GetInt64(lastSeenCutoffMillis),
	}

	if cfg.EIPURL != "" {
		log.Infof("Getting EIP File from: %s", cfg.EIPURL)
	}
	if cfg.EIP != "" {
		log.Infof("Getting EIP File from: %s", cfg.EIP)
	}
	if cfg.ProviderJson == "" {
		log.Errorf("Error: parameter --%s is required.", fromProviderJsonFile)
		log.Errorf("File path to provider.json is missing. Please specify a source by using --%s", fromProviderJsonFile)
		os.Exit(1)
	}

	if cfg.EIPURL == "" && cfg.EIP == "" {
		log.Errorf("Error: No gateways loaded. Please specify a " +
			fmt.Sprintf("gateway source by using --%s or --%s", fromEIPURL, fromEIPFile))
		os.Exit(1)
	}

	if cfg.CaFile == "" {
		log.Errorf("Error: parameter %s is required", caFile)
		os.Exit(1)
	} else if _, err := os.Stat(cfg.CaFile); err != nil {
		log.Errorf("Error: Could not load CaFile. %s", err)
		os.Exit(1)
	} else {
		log.Debugf("Using %s as CaFile", cfg.CaFile)
	}

	// either clientcertURL or else cfg.OvpnCaCrt, cfg.OvpnCaKey, cfg.Algo are required for local cert generation
	if cfg.ClientCertURL != "" {
		log.Infof("Configuring menshen to fetch certs form remote URL: %s", cfg.ClientCertURL)
	} else {
		if cfg.OvpnCaCrt == "" {
			log.Errorf("Error: parameter --%s is required.", ovpnCaCrt)
			log.Errorf("Please specify a file containing the CA certificate required for generating openvpn client certificate.")
			os.Exit(1)
		} else if _, err := os.Stat(cfg.OvpnCaCrt); err != nil {
			log.Errorf("Error: Could not load %s. %s", ovpnCaCrt, err)
			os.Exit(1)
		} else {
			log.Debug(fmt.Sprintf("Using %s as %s", cfg.OvpnCaCrt, ovpnCaCrt))
		}

		if cfg.OvpnCaKey == "" {
			log.Errorf("Error: parameter --%s is required.", ovpnCaKey)
			log.Errorf("Please specify a file containing the CA key required for signing openvpn client certificate.")
			os.Exit(1)
		} else if _, err := os.Stat(cfg.OvpnCaKey); err != nil {
			log.Errorf("Error: Could not load %s. %s", ovpnCaKey, err)
			os.Exit(1)
		} else {
			log.Debugf("Using %s as %s", cfg.OvpnCaKey, ovpnCaKey)
		}

		if cfg.Algo != "ed25519" && cfg.Algo != "ecdsa" && cfg.Algo != "rsa" {
			log.Errorf("Error: parameter --%s %s is not supported.", algo, cfg.Algo)
			log.Errorf("Please specify a supported algo for cert generation. Currently supported algorithms are: ed25519, ecdsa, rsa.")
			os.Exit(1)
		}
	}

	echoMetrics := api.InitMetricsServer()
	go func() {
		msg := fmt.Sprintf("Starting /metrics server on :%d", cfg.PortMetrics)
		log.Infof(msg)
		echoMetrics.Logger.Fatal(echoMetrics.Start(
			fmt.Sprintf(":%d", cfg.PortMetrics)))
	}()

	// TODO pass structured logger
	// TODO defer lb.Stop()
	e := api.InitServer(cfg)

	portStr := fmt.Sprintf(":%d", cfg.Port)
	if cfg.AutoTLS {
		e.Logger.Fatal(e.StartAutoTLS(portStr))
	} else {
		fmt.Println("starting:", portStr)
		e.Logger.Fatal(e.Start(portStr))
	}
}
