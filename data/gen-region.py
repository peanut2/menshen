#!/usr/bin/env python3
#
# This script converts the `countries.csv` original dataset
# into a map[string]*point that can be used from Go.
#
import csv

data = {}


def parseData():
    with open('regions.tsv', newline='') as csvfile:
        reader = csv.DictReader(csvfile, delimiter='\t')
        for row in reader:
            data[row["iso_code"]] = row["economic_region"], row["maxmind_continent"]

pre = """// auto-generated code. Do NOT edit.
package geolocate

type Country struct {
    Region string
    Continent string
}

var regions = map[string]*Country{"""

def genGoSrc():
    print(pre)
    for cc, info in data.items():
          region, continent = info[0], info[1]
          region = region.replace(' ', '_').lower()
          continent = continent.replace(' ', '_').lower()
          if region == '' or continent == '':
              continue
          print(f'\t"{cc}": {{"{region}", "{continent}"}},')
    print('}')


if __name__ == "__main__":
    parseData()
    genGoSrc()
