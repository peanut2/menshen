package storage

import (
	"errors"
	"fmt"
	"strings"

	sqlx "github.com/jmoiron/sqlx"

	_ "github.com/mattn/go-sqlite3"
)

func OpenDatabase(dburi string) (*sqlx.DB, error) {

	if len(dburi) == 0 {
		return nil, errors.New("Could not open database (dburi is empty)")
	}

	// https://www.sqlite.org/wal.html
	// https://www.sqlite.org/foreignkeys.html
	if !strings.Contains(dburi, "?") {
		dburi += "?_foreign_keys=on&_journal=WAL"
	}

	db, err := sqlx.Open("sqlite3", dburi)
	if err != nil {
		return nil, fmt.Errorf("Error opening sqlite db: %w", err)
	}

	// https://github.com/mattn/go-sqlite3/issues/209
	db.SetMaxOpenConns(1)

	_, err = db.Exec(`CREATE TABLE IF NOT EXISTS tokens (
					  key TEXT PRIMARY KEY NOT NULL,
					  buckets TEXT NOT NULL)`)

	if err != nil {
		return nil, fmt.Errorf("Error creating tokens table: %w", err)
	}

	return db, nil
}
