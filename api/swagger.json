{
    "swagger": "2.0",
    "info": {
        "description": "This is a LEAP VPN Service API",
        "title": "Menshen API",
        "termsOfService": "https://leap.se/terms/",
        "contact": {
            "name": "API Support",
            "url": "http://leap.se/support"
        },
        "license": {
            "name": "Apache 2.0",
            "url": "http://www.apache.org/licenses/LICENSE-2.0.html"
        },
        "version": "0.5.2"
    },
    "host": "localhost:1323",
    "basePath": "/",
    "paths": {
        "/api/5/agent/bridge": {
            "put": {
                "security": [
                    {
                        "AgentRegistrationAuth": []
                    }
                ],
                "description": "Register a bridge. This endpoint allows \"menshen agent\" processes running on bridges to register themselves.",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Agent"
                ],
                "summary": "Register a bridge",
                "parameters": [
                    {
                        "description": "bridge",
                        "name": "bridge",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/models.Bridge"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/models.Bridge"
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {}
                    },
                    "404": {
                        "description": "Not Found",
                        "schema": {}
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {}
                    }
                }
            }
        },
        "/api/5/agent/gateway": {
            "put": {
                "security": [
                    {
                        "AgentRegistrationAuth": []
                    }
                ],
                "description": "Register a gateway. This endpoint allows \"menshen agent\" processes running on gateways to register themselves.",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Agent"
                ],
                "summary": "Register a gateway",
                "parameters": [
                    {
                        "description": "gateway",
                        "name": "gateway",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/models.Gateway"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/models.Gateway"
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {}
                    },
                    "404": {
                        "description": "Not Found",
                        "schema": {}
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {}
                    }
                }
            }
        },
        "/api/5/bridge/{location}": {
            "get": {
                "security": [
                    {
                        "BucketTokenAuth": []
                    }
                ],
                "description": "fetch bridges by location",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Provisioning"
                ],
                "summary": "Get Bridges",
                "parameters": [
                    {
                        "type": "string",
                        "description": "Location ID",
                        "name": "location",
                        "in": "path",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "type": "array",
                            "items": {
                                "$ref": "#/definitions/models.Bridge"
                            }
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {}
                    },
                    "404": {
                        "description": "Not Found",
                        "schema": {}
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {}
                    }
                }
            }
        },
        "/api/5/bridges": {
            "get": {
                "security": [
                    {
                        "BucketTokenAuth": []
                    }
                ],
                "description": "Fetch all bridges. This is an optional API endpoint for compatibility with vpnweb, but do not count on all the providers to have it enabled since it makes it easier to enumerate resources. On the other hand, if the service has \"open\" VPN endpoints, they can enumerate them here freely. Bridges, however, should be more restricted as a general rule.",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Provisioning"
                ],
                "summary": "Get All Bridges",
                "parameters": [
                    {
                        "type": "string",
                        "description": "transport (tcp|udp)",
                        "name": "tr",
                        "in": "query"
                    },
                    {
                        "type": "string",
                        "description": "port",
                        "name": "port",
                        "in": "query"
                    },
                    {
                        "type": "string",
                        "description": "type",
                        "name": "type",
                        "in": "query"
                    },
                    {
                        "type": "string",
                        "description": "location",
                        "name": "loc",
                        "in": "query"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "type": "array",
                            "items": {
                                "$ref": "#/definitions/models.Bridge"
                            }
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {}
                    },
                    "404": {
                        "description": "Not Found",
                        "schema": {}
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {}
                    }
                }
            }
        },
        "/api/5/gateway": {
            "get": {
                "security": [
                    {
                        "BucketTokenAuth": []
                    }
                ],
                "description": "Get Gateways with param countrycode for nearest, or with param location to get a gateway in specific location, or a random one without params",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Provisioning"
                ],
                "summary": "Get gateways by location, countrycode or random",
                "parameters": [
                    {
                        "type": "string",
                        "description": "Country code (ISO-2)",
                        "name": "cc",
                        "in": "query"
                    },
                    {
                        "type": "string",
                        "description": "location",
                        "name": "loc",
                        "in": "query"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "type": "array",
                            "items": {
                                "$ref": "#/definitions/models.Gateway"
                            }
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {}
                    },
                    "404": {
                        "description": "Not Found",
                        "schema": {}
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {}
                    }
                }
            }
        },
        "/api/5/gateways": {
            "get": {
                "security": [
                    {
                        "BucketTokenAuth": []
                    }
                ],
                "description": "Fetch all gateways. This is an optional API endpoint for compatibility with vpnweb, but do not count on all the providers to have it enabled since it makes it easier to enumerate resources. On the other hand, if the service has \"open\" VPN endpoints, they can enumerate them here freely. Bridges, however, should be more restricted as a general rule.",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Provisioning"
                ],
                "summary": "Get All Gateways",
                "parameters": [
                    {
                        "type": "string",
                        "description": "base country code (ISO-2)",
                        "name": "cc",
                        "in": "query"
                    },
                    {
                        "type": "string",
                        "description": "transport (tcp|udp)",
                        "name": "tr",
                        "in": "query"
                    },
                    {
                        "type": "string",
                        "description": "port",
                        "name": "port",
                        "in": "query"
                    },
                    {
                        "type": "string",
                        "description": "location override",
                        "name": "loc",
                        "in": "query"
                    },
                    {
                        "type": "boolean",
                        "description": "Sort by load (experimental)",
                        "name": "byload",
                        "in": "query"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "type": "array",
                            "items": {
                                "$ref": "#/definitions/models.Gateway"
                            }
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {}
                    },
                    "404": {
                        "description": "Not Found",
                        "schema": {}
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {}
                    }
                }
            }
        },
        "/api/5/openvpn/cert": {
            "get": {
                "description": "Fetch a new key and cert.",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Provisioning"
                ],
                "summary": "Get openvpn cert",
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "type": "string"
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {}
                    },
                    "404": {
                        "description": "Not Found",
                        "schema": {}
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {}
                    }
                }
            }
        },
        "/api/5/openvpn/config": {
            "get": {
                "description": "fetch a working config file for OpenVPN service.",
                "produces": [
                    "text/plain"
                ],
                "tags": [
                    "Provisioning"
                ],
                "summary": "Fetch OpenVPN Config File.",
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "type": "string"
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {}
                    },
                    "404": {
                        "description": "Not Found",
                        "schema": {}
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {}
                    }
                }
            }
        },
        "/api/5/service": {
            "get": {
                "description": "fetch service information, including location and common tunnel config",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Provisioning"
                ],
                "summary": "Get Service Info",
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/models.EIPService"
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {}
                    },
                    "404": {
                        "description": "Not Found",
                        "schema": {}
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {}
                    }
                }
            }
        },
        "/api/autoconf": {
            "get": {
                "description": "fetch a working config file for OpenVPN service.",
                "produces": [
                    "text/plain"
                ],
                "tags": [
                    "Provisioning"
                ],
                "summary": "Fetch OpenVPN Config File.",
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "type": "string"
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {}
                    },
                    "404": {
                        "description": "Not Found",
                        "schema": {}
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {}
                    }
                }
            }
        },
        "/provider.json": {
            "get": {
                "description": "Fetch provider information how to configure and bootstrap the VPN",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Provisioning"
                ],
                "summary": "Get provider info",
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/models.Provider"
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {}
                    },
                    "404": {
                        "description": "Not Found",
                        "schema": {}
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {}
                    }
                }
            }
        }
    },
    "definitions": {
        "models.Bridge": {
            "type": "object",
            "properties": {
                "auth": {
                    "description": "Any authentication method needed for connect to the bridge, `none`\notherwise.",
                    "type": "string"
                },
                "bucket": {
                    "description": "Bucket is a \"bucket\" tag that connotes a resource group that a user may or may not\nhave access to. An empty bucket string implies that it is open access",
                    "type": "string"
                },
                "experimental": {
                    "description": "An experimental bridge flags any bridge that, for whatever reason,\nis not deemed stable. The expectation is that clients have to opt-in to\nexperimental bridges (and gateways too).",
                    "type": "boolean"
                },
                "healthy": {
                    "description": "Healthy indicates whether this bridge can be used normally.",
                    "type": "boolean"
                },
                "host": {
                    "description": "Host is a unique identifier for the bridge.",
                    "type": "string"
                },
                "ip6_addr": {
                    "description": "IP6Addr is the IPv6 address",
                    "type": "string"
                },
                "ip_addr": {
                    "description": "IPAddr is the IPv4 address",
                    "type": "string"
                },
                "last_seen_millis": {
                    "description": "LastSeenMillis is a unix time in milliseconds representing the last time we received a heartbeat update from this bridge",
                    "type": "integer",
                    "format": "int64"
                },
                "load": {
                    "description": "Load is the fractional load - but for now menshen agent is not measuring\nload in the bridges.",
                    "type": "number"
                },
                "location": {
                    "description": "Location refers to the location to which this bridge points to",
                    "type": "string"
                },
                "options": {
                    "description": "Options contain the map of options that will be passed to the client. It usually\ncontains authentication credentials.",
                    "type": "object",
                    "additionalProperties": {}
                },
                "overloaded": {
                    "description": "Overloaded should be set to true if the fractional load is above threshold.",
                    "type": "boolean"
                },
                "port": {
                    "description": "For some protocols (like hopping) port is undefined.",
                    "type": "integer"
                },
                "transport": {
                    "description": "TCP, UDP or KCP. This was called \"protocol\" before.",
                    "type": "string"
                },
                "type": {
                    "description": "Type of bridge.",
                    "type": "string"
                }
            }
        },
        "models.EIPService": {
            "type": "object",
            "properties": {
                "auth": {
                    "type": "string"
                },
                "locations": {
                    "type": "object",
                    "additionalProperties": {
                        "$ref": "#/definitions/models.Location"
                    }
                },
                "openvpn_configuration": {
                    "type": "object",
                    "additionalProperties": {}
                },
                "serial": {
                    "type": "integer"
                },
                "version": {
                    "type": "integer"
                }
            }
        },
        "models.Gateway": {
            "type": "object",
            "properties": {
                "bucket": {
                    "description": "Bucket is a \"bucket\" tag that connotes a resource group that a user may or may not\nhave access to. An empty bucket string implies that it is open access",
                    "type": "string"
                },
                "experimental": {
                    "description": "An experimental gateway flags any gateway that, for whatever reason,\nis not deemed stable. The expectation is that clients have to opt-in to\nexperimental gateways (and bridges too).",
                    "type": "boolean"
                },
                "healthy": {
                    "description": "Not used now - we could potentially flag gateways that are planned\nto undergo maintenance mode some time in advance.\nWe can also automatically flag as not healthy gateways that appear\nnot to be routing to the internet.",
                    "type": "boolean"
                },
                "host": {
                    "description": "Host is a unique identifier for the gateway host. It does not need to resolve, since\nwe're not using DNS to resolve the gateways.",
                    "type": "string"
                },
                "ip6_addr": {
                    "description": "IP6Addr is the IPv6 address",
                    "type": "string"
                },
                "ip_addr": {
                    "description": "IPAddr is the IPv4 address",
                    "type": "string"
                },
                "last_seen_millis": {
                    "description": "LastSeenMillis is a unix time in milliseconds representing the last time we received a heartbeat update from this gateway",
                    "type": "integer",
                    "format": "int64"
                },
                "load": {
                    "description": "Load is the fractional load received from the menshen agent. For the\ntime being it is a synthethic metric that takes into account number of clients\nand network information for the node.",
                    "type": "number"
                },
                "location": {
                    "description": "Location is the canonical label for the location of the gateway.",
                    "type": "string"
                },
                "overloaded": {
                    "description": "Overloaded should be set to true if the fractional load is above threshold.",
                    "type": "boolean"
                },
                "port": {
                    "description": "The (primary) port this gateway is listening on.",
                    "type": "integer"
                },
                "transport": {
                    "description": "TCP, UDP or KCP. This was called \"protocol\" in previous versions of the API.",
                    "type": "string"
                },
                "type": {
                    "description": "Type is the type of gateway. The only valid type as of 2023 is openvpn.",
                    "type": "string"
                }
            }
        },
        "models.Location": {
            "type": "object",
            "properties": {
                "country_code": {
                    "description": "CountryCode is the two-character country ISO identifier (uppercase).",
                    "type": "string"
                },
                "display_name": {
                    "description": "DisplayName is the user-facing string for a given location.",
                    "type": "string"
                },
                "has_bridges": {
                    "description": "Any location that has at least one bridge configured will set this to true.",
                    "type": "boolean"
                },
                "healthy": {
                    "description": "TODO Not used right now, but intended to signal when a location has all of their\nnodes overwhelmed.",
                    "type": "boolean"
                },
                "hemisphere": {
                    "description": "Hemisphere is a legacy label for a gateway. The rationale was once\nintended to be to allocate gateways for an hemisphere with certain\nregional \"fairness\", even if they're geographically located in a\ndifferent region. We might want to set this on the Gateway or Bridge, not in the\nLocation itself...",
                    "type": "string"
                },
                "label": {
                    "description": "Label is the short representation of a location, used internally.",
                    "type": "string"
                },
                "lat": {
                    "description": "Lat is the latitude for the location.",
                    "type": "string"
                },
                "lon": {
                    "description": "Lon is the longitude for the location.",
                    "type": "string"
                },
                "region": {
                    "description": "Region is the continental region this gateway is assigned to. Not used at the moment,\nintended to use a label from the 7-continent model.",
                    "type": "string"
                },
                "timezone": {
                    "description": "Timezone is the TZ for the location (-1, 0, +1, ...)",
                    "type": "string"
                }
            }
        },
        "models.Provider": {
            "type": "object",
            "properties": {
                "api_uri": {
                    "description": "URL of the API endpoints",
                    "type": "string"
                },
                "api_version": {
                    "description": "oldest supported api version\ndeprecated: kept for backwards compatibility. Replaced by api_versions.",
                    "type": "string"
                },
                "api_versions": {
                    "description": "all API versions the provider supports",
                    "type": "array",
                    "items": {
                        "type": "string"
                    }
                },
                "ask_for_donations": {
                    "description": "Flag indicating whether to show regularly a donation reminder",
                    "type": "boolean"
                },
                "ca_cert_fingerprint": {
                    "description": "fingerprint of CA cert used to setup TLS sessions during VPN setup (and up to API version 3 for API communication)\ndeprecated: kept for backwards compatibility",
                    "type": "string"
                },
                "ca_cert_uri": {
                    "description": "URL to fetch the CA cert used to setup TLS sessions during VPN setup (and up to API version 3 for API communication)\ndeprecated: kept for backwards compatibility",
                    "type": "string"
                },
                "country_code_lookup_url": {
                    "description": "URL of a service that returns a country code for an ip address. If empty,\nOONI backend is used",
                    "type": "string"
                },
                "default_language": {
                    "description": "Default language this provider uses to show infos and provider messages",
                    "type": "string"
                },
                "description": {
                    "description": "Short description about the provider",
                    "type": "object",
                    "additionalProperties": {
                        "type": "string"
                    }
                },
                "domain": {
                    "description": "Domain of the provider",
                    "type": "string"
                },
                "donate_period": {
                    "description": "Number of days until a donation reminder reappears",
                    "type": "string"
                },
                "donate_url": {
                    "description": "URL to the donation website",
                    "type": "string"
                },
                "info_url": {
                    "description": "URL to general provider website",
                    "type": "string"
                },
                "languages": {
                    "description": "Languages the provider supports to show infos and provider messages",
                    "type": "array",
                    "items": {
                        "type": "string"
                    }
                },
                "motd_url": {
                    "description": "URL to the message of the day service",
                    "type": "string"
                },
                "name": {
                    "description": "Provider name",
                    "type": "object",
                    "additionalProperties": {
                        "type": "string"
                    }
                },
                "service": {
                    "description": "Operational properties which describe how the provider offers the service",
                    "type": "object",
                    "properties": {
                        "allow_anonymous": {
                            "description": "Flag indicating if anonymous usage without registration is allowed\ndeprecated: kept for backwards compatibility",
                            "type": "boolean"
                        },
                        "allow_registration": {
                            "description": "Flag indicating if the provider supports user registration\ndeprecated: kept for backwards compatibility",
                            "type": "boolean"
                        }
                    }
                },
                "services": {
                    "description": "List of services the provider offers, currently only openvpn",
                    "type": "array",
                    "items": {
                        "type": "string"
                    }
                },
                "stun_servers": {
                    "description": "list of STUN servers (format: ip/hostname:port) servers to get current ip address\ncan consist of self hosted STUN servers, public ones or a combination of both.\nGeolocationLookup is only done when the list of STUNServers is not empty",
                    "type": "array",
                    "items": {
                        "type": "string"
                    }
                },
                "tos_url": {
                    "description": "URL to Terms of Service website",
                    "type": "string"
                }
            }
        }
    },
    "securityDefinitions": {
        "AgentRegistrationAuth": {
            "description": "This is an HMAC based webhook authentication that uses a shared private key to ensure the authenticity of agent registration messages.",
            "type": "apiKey",
            "name": "x-menshen-agent-auth",
            "in": "header"
        },
        "BucketTokenAuth": {
            "description": "This is an auth token that corresponds to access to particular resources which are delineated by \"buckets\".",
            "type": "apiKey",
            "name": "x-menshen-auth-token",
            "in": "header"
        }
    }
}